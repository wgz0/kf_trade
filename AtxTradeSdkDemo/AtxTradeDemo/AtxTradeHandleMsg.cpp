﻿#include "AtxTradeHandleMsg.h"
#include <string>

//获取当前时间
static void getCurrTime(int* currdate, int* currtime, int* currmillisecond, int* curryear)
{
	auto t = std::chrono::system_clock::now();
	long long microsec = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
	time_t localt = std::chrono::system_clock::to_time_t(t);
	struct tm * tm = localtime(&localt);
	if (currdate != NULL)
	{
		*currdate = ((tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday) % 10000000;
	}
	if (currtime != NULL)
	{
		*currtime = tm->tm_hour * 100 + tm->tm_min;
	}
	if (currmillisecond != NULL)
	{
		*currmillisecond = (int)(microsec / 1000000 % 1000000);
	}
    if (curryear != NULL)
    {
        *curryear = tm->tm_year + 1900;
    }
}

static int getCurrdate()
{
	auto t = std::chrono::system_clock::now();
	long long microsec = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
	time_t localt = std::chrono::system_clock::to_time_t(t);
	struct tm * tm = localtime(&localt);
	int currdate = ((tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday) % 10000;

	return currdate;
}

int currdate = getCurrdate();
static int requestid = 0;

//建立连接成功后回调
void HandleMsg::OnConnect()
{
    m_connect = true;
    printf("HandleMsg::OnConnect success\n");
}

//连接断开时回调
void HandleMsg::OnDisconect()
{
    m_connect = false;
    m_create = false;
    printf("HandleMsg::OnDisconect success\n");
}

int HandleMsg::StartDumpData()
{
    std::thread dth(&HandleMsg::ThreadDumpData, this);
    m_dumpData_th.swap(dth);
    return 0;
}

void HandleMsg::ThreadDumpData()
{
    while (1)
    {
        if (!m_connect)
        {
            if (pApi && !m_create)
            {
                pApi->release();
                pApi = nullptr;
                pApi = X2ClientTradeApi::CreateTradeApi();
                pApi->RegisterSpi(this);
                m_create = true;
            }
            bool ret = pApi->Connect();
            if (ret)
            {
                m_connect = true;
            }
        }
#ifdef WIN32    
        Sleep(3000);
#else
        usleep(30000);
#endif
    }
}

//登录应答
void HandleMsg::OnRspLogin(X2LoginRsp* pRspLogin, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		printf("OnRspLogin receive success\n");
	}
	else	//	其余为失败
	{
		printf("OnRspLogin receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//资金账号查询应答
void HandleMsg::OnRspQueryAccountInfo(X2FundAccountInfoRsp* pRspAccount, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspQueryAccountInfo receive success size[%d] accountId[%d] accountUser[%s] accountName[%s] accountType[%d] assetProp[%d] accountStatus[%d]\n",
				i, pRspAccount->accountId, pRspAccount->accountUser, pRspAccount->accountName,
				(int)pRspAccount->accountType, (int)pRspAccount->assetProp, pRspAccount->accountStatus);
			pRspAccount++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspQueryAccountInfo receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

void HandleMsg::OnRspQueryAccountId(X2QueryAccountIdRsp* pRspQueryAccountId, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        printf("OnRspQueryAccountId receive success accountId[%d]", pRspQueryAccountId->accountId);

    }
    else	//	其余为失败
    {
        printf("OnRspQueryAccountId receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

void HandleMsg::OnRspAccountChangePwd(X2AccountChangePwdRsp* pAccountChangePwd, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		printf("OnRspAccountChangePwd receive success\n");
	}
	else	//	其余为失败
	{
		printf("OnRspAccountChangePwd receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//账户资金查询应答
void HandleMsg::OnRspBalance(X2BalanceRsp* pBalance, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		printf("OnRspBalance receive\n");
	}
	else	//	其余为失败
	{
		printf("OnRspBalance receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//股票持仓查询应答
void HandleMsg::OnRspStockPosition(X2StockPositionRsp* pPosition, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspStockPosition symbol[%s] openQty[%d] holdQty[%d] availQty[%d] lastPx[%lf] costPx[%lf] profitAmt[%lf] queryPos[%s] receive success\n",
                pPosition->symbol, pPosition->openQty, pPosition->holdQty, pPosition->availQty, pPosition->lastPx, pPosition->costPx, pPosition->profitAmt, pPosition->queryPos);
			pPosition++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspStockPosition receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//子单委托回报主推
void HandleMsg::OnRtnOrderReport(X2OrderActualReport* orderReport)
{
    printf("OnRtnOrderReport receive\n");
}

//订阅主题响应
void HandleMsg::OnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID)
{
	printf("OnSubTopic receive\n");
}

//订阅主题响应
void HandleMsg::OnUnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID)
{
	printf("OnUnSubTopic receive\n");
}

//子单成交回报主推
void HandleMsg::OnRtnTradeReport(X2TradeActualReport* tradeReport)
{
	printf("OnRtnTradeReport receive\n");
}

//母单委托回报主推
void HandleMsg::OnRtnInstructionReport(X2ClientInstructionReport* pReport)
{
    printf("OnRtnInstructionReport receive\n");
    //printf("OnRtnInstructionReport:traderId[%d] algoId[%d] algoTypeId[%d] algoTypeName[%s] orgId[%d]\
    //        accountId[%d] accountUser[%s] symbol[%s] marketType[%d] effectiveTime[%lld] \
    //        expireTime[%lld] sysQuoteId[%lld] transacTime[%d] algoParam[%s] taskQty[%d] \
    //        cumQty1[%d] cumQty2[%d] outstandingQty1[%d] outstandingQty2[%d] leavesQty1[%d] \
    //        leavesQty2[%d] taskAmt[%lf] cumAmt1[%lf] cumAmt2[%lf] note[%s] \
    //        updateTime[%d] orderStatus[%d] basketId[%s] success\n",
    //    pReport->traderId, pReport->algoId, pReport->algoTypeId, pReport->algoTypeName, pReport->orgId,
    //    pReport->accountId, pReport->accountUser, pReport->symbol, (int)pReport->marketType, pReport->effectiveTime,
    //    pReport->expireTime, pReport->sysQuoteId, pReport->transacTime, pReport->algoParam, pReport->taskQty,
    //    pReport->cumQty1, pReport->cumQty2, pReport->outstandingQty1, pReport->outstandingQty2, pReport->leavesQty1,
    //    pReport->leavesQty2, pReport->taskAmt, pReport->cumAmt1, pReport->cumAmt2, pReport->note,
    //    pReport->updateTime, (int)pReport->orderStatus, pReport->basketId);
}

//子单订单查询应答
void HandleMsg::OnRspQueryOrderActual(X2OrderActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspQueryOrderActual receive\n");
			pRspActual++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspQueryOrderActual receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//母单订单查询应答
void HandleMsg::OnRspQueryOrderInstruction(X2ClientInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspQueryOrderInstruction receive\n");
			pRspInstruction++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspQueryOrderInstruction receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//算法类型查询应答
void HandleMsg::OnRspQueryAlgoType(X2ClientQueryAtxInfoRsp* pRspAtx, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspQueryAlgoType requestID[%d] traderId[%d] accountId[%d] accountUser[%s] algoTypeId[%d] algoTypeName[%s] algoId[%d] algoName[%s] orgId[%d] algoTypeFieldsInfo[%s] receive\n",
                nRequestID, pRspAtx->traderId, pRspAtx->accountId, pRspAtx->accountUser, pRspAtx->algoTypeId, pRspAtx->algoTypeName, pRspAtx->algoId, pRspAtx->algoName, pRspAtx->orgId, pRspAtx->algoTypeFieldsInfo);
			pRspAtx++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspQueryAlgoType receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//母单下单应答
void HandleMsg::OnRspInstructionCreate(X2ClientInstructionCreateRsp* pRspCreate, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspInstructionCreate sysQuoteId[%lld] clQuoteId[%s]  basketId[%s] receive\n",
                pRspCreate->sysQuoteId, pRspCreate->clQuoteId, pRspCreate->basketId);
			pRspCreate++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspInstructionCreate receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//母单撤单应答
void HandleMsg::OnRspInstructionCancel(X2ClientInstructionCancelRsp* pRspCancel, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspInstructionCancel receive\n");
			pRspCancel++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspInstructionCancel receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//母单改单应答
void HandleMsg::OnRspInstructionReplace(X2ClientInstructionReplaceRsp* pRspReplace, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspInstructionReplace receive\n");
			pRspReplace++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspInstructionReplace receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//母单暂停/恢复应答
void HandleMsg::OnRspInstructionControl(X2ClientInstructionControlRsp* pRspControl, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
	// 首先判断返回结果是否成功
	if (pRspInfo->errCode == 0)	//	success
	{
		for (int i = 0; i < nCount; i++)
		{
			printf("OnRspInstructionControl receive\n");
			pRspControl++;
		}
	}
	else	//	其余为失败
	{
		printf("OnRspInstructionControl receive failed errCode[%d] errMsg[%s]\n",
			pRspInfo->errCode, pRspInfo->errMsg);
	}
}

//结算子单查询
void HandleMsg::OnRspSettleActualQry(X2SettleActualQryRsp* pSettleActualQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    //若nCount(返回数量)小于请求的单次最大数量(queryNum),说明已查询完成,否则继续查询,定位串使用最后一次返回的queryPos
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleActualQry receive\n");
            pSettleActualQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleActualQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//结算母单查询
void HandleMsg::OnRspSettleInstructionQry(X2SettleInstructionQryRsp* pSettleSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    //若nCount(返回数量)小于请求的单次最大数量(queryNum),说明已查询完成,否则继续查询,定位串使用最后一次返回的queryPos
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleInstructionQry receive\n");
            pSettleSummaryQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleInstructionQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//结算汇总查询
void HandleMsg::OnRspSettleSummaryQry(X2SettleSummaryQryRsp* pSettleInstructionQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleInstructionQry receive\n");
            pSettleInstructionQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleInstructionQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//结算篮子汇总查询
void HandleMsg::OnRspSettleBasketSummaryQry(X2SettleBasketSummaryQryRsp* pSettleBasketSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleBasketSummaryQry receive\n");
            pSettleBasketSummaryQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleBasketSummaryQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//结算T0汇总查询
void HandleMsg::OnRspSettleT0SummaryQry(X2SettleT0SummaryQryRsp* pSettleT0SummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleT0SummaryQry receive\n");
            pSettleT0SummaryQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleT0SummaryQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//结算隔夜仓查询
void HandleMsg::OnRspSettleT0OverNightQry(X2SettleT0OverNightQryRsp* pSettleT0OverNightQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspSettleT0OverNightQry receive\n");
            pSettleT0OverNightQry++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspSettleT0OverNightQry receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//子单成交查询应答
void HandleMsg::OnRspQueryDealActual(X2DealActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspQueryDealActual receive\n");
            pRspActual++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspQueryDealActual receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//母单历史订单查询应答
void HandleMsg::OnRspQueryHistoryOrderInstruction(X2ClientHistoryInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRspQueryHistoryOrderInstruction receive\n");
            pRspInstruction++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRspQueryHistoryOrderInstruction receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

//一地多户查询应答
void HandleMsg::OnRsQueryAcctIdsByAcctUser(X2AcctIdsByAcctUserQueryRsp* pAcctIdsByAcctUser, int nCount, X2RspInfoField* pRspInfo, int nRequestID)
{
    // 首先判断返回结果是否成功
    if (pRspInfo->errCode == 0)	//	success
    {
        for (int i = 0; i < nCount; i++)
        {
            printf("OnRsQueryAcctIdsByAcctUser receive\n");
            pAcctIdsByAcctUser++;
        }
    }
    else	//	其余为失败
    {
        printf("OnRsQueryAcctIdsByAcctUser receive failed errCode[%d] errMsg[%s]\n",
            pRspInfo->errCode, pRspInfo->errMsg);
    }
}

HandleMsg::HandleMsg()
{
    pApi = nullptr;
    pApi = X2ClientTradeApi::CreateTradeApi();
	pApi->RegisterSpi(this);
    //auto_connect=false 需手动连接
    if (pApi)
    {
        bool ret = pApi->Connect();
        if (ret)
        {
            m_connect = true;
            printf("connect success");
        }
    }

}

HandleMsg::~HandleMsg()
{
}

void HandleMsg::InitHandleMsg(HandleMsg *p)
{
	X2LoginReq req;
	req.traderId = atoi(p->traderId.c_str()); //交易员编号
	strcpy(req.password, p->password.c_str());
	p->Login(&req);

	{
		X2Topic topic;
		topic.accountId = atoi(p->accountId.c_str());	//账户编号
		topic.type = ReportType::REPORT_TYPE_DEAL;	//订阅成交
		p->Subscribe(&topic);
	}

	{
		X2Topic topic2;
		topic2.accountId = atoi(p->accountId.c_str());	//账户编号
		topic2.type = ReportType::REPORT_TYPE_ORDER;	//订阅委托
		p->Subscribe(&topic2);
	}

	{
		X2Topic topic3;
		topic3.accountId = atoi(p->accountId.c_str());	//账户编号
		topic3.type = ReportType::REPORT_TYPE_ALGOORDER;	//订阅母单
		p->Subscribe(&topic3);
	}
}

// 母单下单请求接口实现01
void HandleMsg::ReqInstructionCreate()
{
    int currtime = 0;
    int currmillisecond = 0;
    int curryear = 0;
    int currdate = getCurrdate();
    getCurrTime(NULL, &currtime, &currmillisecond, &curryear);
    char c_effectiveTime[64] = { 0 };
    char c_expireTime[64] = { 0 };
    if (currdate < 1000)
    {
        sprintf(c_effectiveTime, "%d0%d095400000", curryear, currdate);
        sprintf(c_expireTime, "%d0%d095800000", curryear, currdate);
    }
    else
    {
        sprintf(c_effectiveTime, "%d%d095400000", curryear, currdate);
        sprintf(c_expireTime, "%d%d095800000", curryear, currdate);
    }

    //std::cout << currdate << std::endl;           // 509
    //std::cout << c_effectiveTime << std::endl;    // 20230509091500000
    //std::cout << c_expireTime << std::endl;       // 20230509092500000


    X2ClientInstructionCreate pInstructionCreate;

    char str[80] = { 0 };
    printf("请输入机构编号:");
    scanf("%s", str);
    pInstructionCreate.orgId = atoi(str);
    printf("请输入算法类型编号:");
    scanf("%s", str);
    pInstructionCreate.algoTypeId = atoi(str);
    printf("请输入算法编号:");
    scanf("%s", str);
    pInstructionCreate.algoId = atoi(str);
    printf("请输入账户编号:");
    scanf("%s", str);
    pInstructionCreate.accountId = atoi(str);
    printf("请输入证券代码:");
    scanf("%s", str);
    strcpy(pInstructionCreate.symbol, str);
    printf("请输入市场类型(1-深证 2-上海 3-中金所 4-上期所 5-大连商品 6-郑州商品 7-能源):");
    scanf("%s", str);
    pInstructionCreate.marketType = (AtxMarketType)atoi(str);

    pInstructionCreate.effectiveTime = atoll(c_effectiveTime);	//开始时间
    pInstructionCreate.expireTime = atoll(c_expireTime);	//结束时间
    sprintf(pInstructionCreate.clQuoteId, "%d%d%d%d%d", curryear, currdate, currtime, currmillisecond, requestid);	//三方编号
    sprintf(pInstructionCreate.basketId, "%d%d%d%d%d", curryear, currdate, currtime, currmillisecond, requestid);
    std::string tmp = "{\"side\":1,\"price\":7,\"task_qty\":1000,\"limit_action\":0,\"after_action\":0}";
    strcpy(pInstructionCreate.algoParam, tmp.c_str());	//自定义参数
    strcpy(pInstructionCreate.opstation, "");	//站点信息
    InstructionCreate(&pInstructionCreate, 1);
}



// 母单下单请求接口实现02
void HandleMsg::ReqInstructionCreateNo2(char const* &algoTypeId,
                                        char const* &orgId,
                                        char const* &accountId,
                                        std::string symbol,
                                        char const* &marketType,
                                        char const* &effectiveTime,
                                        char const* &expireTime,
                                        char const* &clQuoteId,
                                        std::string algoParam,
                                        char const* &algoId,
                                        char const* &basketId,
                                        char const* &opstation)
{   
    /*
    * 算法类型编号:algoTypeId[102]
      机构编号:orgId[0]
      账户编号:accountId[104000001]
      证券代码:symbol[000002]
      市场类型:marketType[1]
      开始时间:effectiveTime[20230509091500000]
      结束时间:expireTime[20230509092500000]
      三方编号:clQuoteId[202350915218905425]
      自定义参数:algoParam[{"side":1,"price":7,"task_qty":1000,"limit_action":0,"after_action":0}]
      算法编号:algoId[102]
      篮子编号:basketId[202350915218905425]
      站点信息:opstation       
    */

    /*
    入参：
        param1、算法类型编号:algoTypeId[102]
        param2、机构编号: orgId[0]
        param3、账户编号: accountId[104000001]
        param4、证券代码: symbol[000001]
        param5、市场类型(1-深证 2-上海 3-中金所 4-上期所 5-大连商品 6-郑州商品 7-能源): marketType[1]
        param4、算法编号: algoId[102]
    */

    int currtime = 0;
    int currmillisecond = 0;
    int curryear = 0;
    int currdate = getCurrdate();
    getCurrTime(NULL, &currtime, &currmillisecond, &curryear);
    char c_effectiveTime[64] = { 0 };
    char c_expireTime[64] = { 0 };
    if (currdate < 1000)
    {
        sprintf(c_effectiveTime, "%d0%d091500000", curryear, currdate);
        sprintf(c_expireTime,    "%d0%d092500000", curryear, currdate);
    }
    else
    {
        sprintf(c_effectiveTime, "%d%d091500000", curryear, currdate);
        sprintf(c_expireTime,    "%d%d092500000", curryear, currdate);
    }

    X2ClientInstructionCreate pInstructionCreate;

    pInstructionCreate.orgId = atoi(orgId);           // 机构编号
    pInstructionCreate.algoTypeId = atoi(algoTypeId); // 算法类型编号
    pInstructionCreate.algoId = atoi(algoId);         // 算法编号
    pInstructionCreate.accountId = atoi(accountId);   // 账户编号


    const char* t_symbol = symbol.c_str();
    strcpy(pInstructionCreate.symbol, t_symbol);

    // "请输入市场类型(1-深证 2-上海 3-中金所 4-上期所 5-大连商品 6-郑州商品 7-能源)"
    pInstructionCreate.marketType = (AtxMarketType)atoi(marketType);  //市场类型

    pInstructionCreate.effectiveTime = atoll(effectiveTime);	      //开始时间
    pInstructionCreate.expireTime = atoll(expireTime);	              //结束时间

    sprintf(pInstructionCreate.clQuoteId, "%d%d%d%d%d", curryear, currdate, currtime, currmillisecond, requestid);	//三方编号
    sprintf(pInstructionCreate.basketId,  "%d%d%d%d%d", curryear, currdate, currtime, currmillisecond, requestid);

    strcpy(pInstructionCreate.algoParam, algoParam.c_str());	      //自定义参数
    strcpy(pInstructionCreate.opstation, "");	                      //站点信息
    InstructionCreate(&pInstructionCreate, 1);
}

// 账户登录
long long HandleMsg::Login(X2LoginReq* pLogin)
{
    return pApi->Login(pLogin, requestid++);
}

// 账户查询
long long HandleMsg::QueryAccount()
{
    // 资金账号查询
	return pApi->ReqQueryAccountInfo(requestid++);
}

long long HandleMsg::QueryAccountId(X2QueryAccountIdReq* pQueryAccountId)
{
    return pApi->ReqQueryAccountId(pQueryAccountId, requestid++);
}

long long HandleMsg::Balance(X2Balance* pBalance)
{
	return pApi->ReqBalance(pBalance, requestid++);
}

long long HandleMsg::StockPosition(X2StockPosition* pPosition)
{
	return pApi->ReqStockPosition(pPosition, requestid++);
}

long long HandleMsg::Subscribe(X2Topic* topic)
{
	return pApi->SubscribeReport(topic, requestid++);
}

long long HandleMsg::UnSubscribe(X2Topic* topic)
{
	return pApi->UnSubscribeReport(topic, requestid++);
}

long long HandleMsg::QueryOrderActual(X2OrderActualQuery* pOrderActual)
{
	return pApi->ReqQueryOrderActual(pOrderActual, requestid++);
}

long long HandleMsg::QueryOrderInstruction(X2ClientInstructionQuery* pOrderInstruction)
{
	return pApi->ReqQueryOrderInstruction(pOrderInstruction, requestid++);
}

long long HandleMsg::QueryAlgoType(X2ClientQueryAtxInfo* pAlgo)
{
	return pApi->ReqQueryAlgoType(pAlgo, requestid++);
}

long long HandleMsg::InstructionCreate(X2ClientInstructionCreate* pInstructionCreate, int nCount)
{
	return pApi->ReqInstructionCreate(pInstructionCreate, requestid++, nCount);
}

long long HandleMsg::InstructionCancel(X2ClientInstructionCancel* pInstructionCancel, int nCount)
{
	return pApi->ReqInstructionCancel(pInstructionCancel, requestid++, nCount);
}

long long HandleMsg::InstructionReplace(X2ClientInstructionReplace* pInstructionReplace, int nCount)
{
	return pApi->ReqInstructionReplace(pInstructionReplace, requestid++, nCount);
}

long long HandleMsg::InstructionControl(X2ClientInstructionControl* pInstructionControl, int nCount)
{
	return pApi->ReqInstructionControl(pInstructionControl, requestid++, nCount);
}

long long HandleMsg::AccountChangePwdReq(X2AccountChangePwdReq* pAccountChangePwd)
{
	return pApi->ReqAccountChangePwd(pAccountChangePwd, requestid++);
}

long long HandleMsg::SettleActualQry(X2SettleActualQryReq* pSettleActualQry)
{
	return pApi->ReqSettleActualQry(pSettleActualQry, requestid++);
}

long long HandleMsg::SettleInstructionQry(X2SettleInstructionQryReq* pSettleSummaryQry)
{
    return pApi->ReqSettleInstructionQry(pSettleSummaryQry, requestid++);
}

long long HandleMsg::SettleSummaryQry(X2SettleSummaryQryReq* pSettleInstructionQry)
{
    return pApi->ReqSettleSummaryQry(pSettleInstructionQry, requestid++);
}

long long HandleMsg::SettleBasketSummaryQry(X2SettleBasketSummaryQryReq* pSettleBasketSummaryQry)
{
    return pApi->ReqSettleBasketSummaryQry(pSettleBasketSummaryQry, requestid++);
}

long long HandleMsg::SettleT0SummaryQry(X2SettleT0SummaryQryReq* pSettleT0SummaryQry)
{
    return pApi->ReqSettleT0SummaryQry(pSettleT0SummaryQry, requestid++);
}

long long HandleMsg::SettleT0OverNightQry(X2SettleT0OverNightQryReq* pSettleT0OverNightQry)
{
    return pApi->ReqSettleT0OverNightQry(pSettleT0OverNightQry, requestid++);
}

long long HandleMsg::QueryTradeActual(X2TradeActualQuery* pTradeActual)
{
    return pApi->ReqQueryTradeActual(pTradeActual, requestid++);
}
