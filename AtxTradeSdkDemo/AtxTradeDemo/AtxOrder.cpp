// dllmain.cpp : 定义 DLL 应用程序的入口点。

#include "AtxTradeHandleMsg.h"
#include <xlnt/xlnt.hpp>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <typeinfo>


using namespace std;


// 1.1、获取excel表对象
xlnt::worksheet readExcel(const std::string& str) {
	xlnt::workbook wb;
	wb.load(str);
	xlnt::worksheet ws = wb.active_sheet();
	return ws;
}

// 1.2、获取csv表对象
vector<vector<string>> readCSV(const std::string& pathName) {
	ifstream inFile(pathName, ios::in);
	string lineStr;
	vector<vector<string>> strArray;
	while (getline(inFile, lineStr))
	{
		// 打印整行字符串
		//cout << lineStr << endl;
		// 存成二维表结构
		stringstream ss(lineStr);
		string str;
		vector<string> lineArray;
		// 按照逗号分隔
		while (getline(ss, str, ','))
			lineArray.push_back(str);
		strArray.push_back(lineArray);
	}
	return strArray;
}

// 1.3、股票代码复原
string getStockCode(string originCode)
{
	string tmp1;
	string std_code = "000000";
	tmp1 = std_code.substr(0, 6 - originCode.length());
	return tmp1 + originCode;
}

// 1.4、获取当前日期
int getCurrdateNum()
{
	// eg: 509、510
	auto t = std::chrono::system_clock::now();
	long long microsec = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
	time_t localt = std::chrono::system_clock::to_time_t(t);
	struct tm* tm = localtime(&localt);
	int currdate = ((tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday) % 10000;
	return currdate;
}

// 1.5、获取当前时间点
void getCurrTimeNum(int* currdate, int* currtime, int* currmillisecond, int* curryear)
{
	auto t = std::chrono::system_clock::now();
	long long microsec = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
	time_t localt = std::chrono::system_clock::to_time_t(t);
	struct tm* tm = localtime(&localt);
	if (currdate != NULL)
	{
		*currdate = ((tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday) % 10000000;
	}
	if (currtime != NULL)
	{
		*currtime = tm->tm_hour * 100 + tm->tm_min;
	}
	if (currmillisecond != NULL)
	{
		*currmillisecond = (int)(microsec / 1000000 % 1000000);
	}
	if (curryear != NULL)
	{
		*curryear = tm->tm_year + 1900;
	}
}

// 1.6、获取当前日期字符串
string getCurrdateString()
{
	// eg: 0509、0510、1112
	auto t = std::chrono::system_clock::now();
	long long microsec = std::chrono::duration_cast<std::chrono::nanoseconds>(t.time_since_epoch()).count();
	time_t localt = std::chrono::system_clock::to_time_t(t);
	struct tm* tm = localtime(&localt);
	int currdate = ((tm->tm_year + 1900) * 10000 + (tm->tm_mon + 1) * 100 + tm->tm_mday) % 10000;
	char curdatestr[64] = { 0 };
	if (currdate < 1000)
	{
		sprintf(curdatestr, "0%d", currdate);
	}
	else
	{
		sprintf(curdatestr, "%d", currdate);
	}
	return curdatestr;
}


// 主函数
int main(int argc, char* argv[])
{
#ifdef WIN32
	setlocale(LC_CTYPE, "");
#else
#endif
	/***********************[一、参数修改区域]**************************/
	string datestr    = getCurrdateString();// 确定表格日期 0509\0522

	char ord_sttime[] = "0930";             // 起始时间
	char ord_ettime[] = "1030";             // 结束时间

	char t0_ord_sttime[] = "0930";          // T0交易起始时间
	char t0_ord_ettime[] = "1450";          // T0交易结束时间

	/***********************[二、参数修改区域]**************************/
	// 获取时间字段
	int cur_year = 0;                 // 年份 eg: 2023
	int cur_date = getCurrdateNum();  // 日期 eg: 510
	int cur_time = 0;                 // 小时 eg: 1051
	int cur_millisecond = 0;          // 秒数 eg: 70979
	getCurrTimeNum(NULL, &cur_time, &cur_millisecond, &cur_year);

	char tt_effectiveTime[64] = { 0 };
	char tt_expireTime[64]    = { 0 };

	char t0_tt_effectiveTime[64] = { 0 };
	char t0_tt_expireTime[64]    = { 0 };

	if (cur_date < 1000)
	{
		sprintf(tt_effectiveTime, "%d0%d%s00000", cur_year, cur_date, ord_sttime);
		sprintf(tt_expireTime   , "%d0%d%s00000", cur_year, cur_date, ord_ettime);

		sprintf(t0_tt_effectiveTime, "%d0%d%s00000", cur_year, cur_date, t0_ord_sttime);
		sprintf(t0_tt_expireTime   , "%d0%d%s00000", cur_year, cur_date, t0_ord_ettime);
	}
	else
	{
		sprintf(tt_effectiveTime, "%d%d%s00000", cur_year, cur_date, ord_sttime);
		sprintf(tt_expireTime   , "%d%d%s00000", cur_year, cur_date, ord_ettime);

		sprintf(t0_tt_effectiveTime, "%d%d%s00000", cur_year, cur_date, t0_ord_sttime);
		sprintf(t0_tt_expireTime   , "%d%d%s00000", cur_year, cur_date, t0_ord_ettime);
	}

	char const* t_effectiveTime = tt_effectiveTime;    // 开始时间
	char const* t_expireTime    = tt_expireTime;       // 结束时间

	char const* t0_effectiveTime = t0_tt_effectiveTime;    // T0交易开始时间
	char const* t0_expireTime    = t0_tt_expireTime;       // T0交易结束时间


	// 1、指针p指向HandleMsg实例化对象
	HandleMsg* p = new HandleMsg();
	p->StartDumpData();
	char str[80] = { 0 };
	cout << endl;


	// 2、获取买卖下单文件数据
	vector< vector<string> > BuyInfoArray;
	vector< vector<string> > SellInfoArray;
	string BuyPathName  = "./dataSource/默认篮子格式买" + datestr + ".csv";
	string SellPathName = "./dataSource/默认篮子格式卖" + datestr + ".csv";
	BuyInfoArray = readCSV(BuyPathName);
	SellInfoArray = readCSV(SellPathName);

	string Bcode, Bv2, Bvolume, Bweight, Bside;
	string Scode, Sv2, Svolume, Sweight, Sside;
	string Tcode, Tv2, Tvolume, Tname, Tside;
	int m, i;


	// 3、下单
	while(1)
	{
		printf("=======================<<<模拟盘>>>========================\n");
		printf("下单模拟操作\n");
		printf("1.登录\n");
		printf("2.资金账号查询\n");
		printf("3.账户资金查询\n");
		printf("4.股票持仓查询\n");
		printf("5.回报订阅\n");
		printf("6.取消订阅\n");
		printf("7.子单订单查询\n");
		printf("8.母单订单查询\n");
		printf("9.算法类型查询\n");
		printf("10.主动算法下单请求+++++++++++++++++++++++++++++++++++++++\n");
		printf("11.母单撤单请求\n");
		printf("12.母单改单请求\n");
		printf("13.母单暂停请求\n");
		printf("14.母单恢复请求\n");
		printf("15.修改资金账号密码\n");
		printf("16.通过资金资产账户查询资金账号\n");
		printf("17.结算子单查询\n");
		printf("18.结算母单查询\n");
		printf("19.结算汇总查询\n");
		printf("20.结算篮子汇总查询\n");
		printf("21.T0策略——(趋势策略)下单请求+++++++++++++++++++++++++++++\n");
		printf("22.结算T0汇总查询\n");
		printf("23.T0策略——(高频策略)下单请求+++++++++++++++++++++++++++++\n");
		printf("24.结算隔夜仓查询\n");
		printf("25.子单成交查询\n");
		printf("请输入相应操作对应的数字:\n");
		printf("确认主动下单算法与T0下单算法无误后,输入下单编号进行下单执行!\n");

		// 定义字符数组存储字符串
		char str[80];
		scanf("%s", str);

		switch (atoi(str))
		{
		case 1:{  // 1.登录账户
			X2LoginReq req;
			
			printf("输入交易员: 10400001\n");
			string str = "10400001";
			req.traderId = atoi(str.c_str());  //交易员编号:10400001

			printf("输入交易员密码: a12345\n");
			str = "a12345";
			strcpy(req.password, str.c_str()); //交易员密码:a12345
			
			strcpy(req.opstation, "PC;IIP=xxx.xxx.xxx.xxx;IPORT=xxxxx;LIP=xxx.xxx.xxx.xxx;MAC=xxxxxxxxxxxx;HD=xxxxxxxxxxxx;PCN=xxxxxxxxxxxx;CPU=xxxxxxxxxxxx;PI=xxxxxxxxxxxx;VOL=xxxxxxxxxxxx");

			p->Login(&req);   //  case1:进行登录
			break;
		}
		case 2:{ // 2.资金账号查询
			/*
			* 资金账号查询应答:
				账户编号: accountId[104000001]
				资金账户：accountUser[202304181002]
				账号名称：accountName[jltest]
				账户类型：accountType[1] 
				资产属性：assetProp[0]
				账户状态：accountStatus[1]
			*/
			p->QueryAccount(); 
			break;
		}
		case 3:{ // 3.账户资金查询
			/*
			* 账户资金查询应答
			*	账户编号: accountId[100000000]
			*	资金账号：accountUser[0000000000]
			*	资金类型：fundKind[1]
			*	可用资金：availAmt[0.000000]
			*   可取资金：drawAmt[0.000000]
			*   资金余额：openAmt[0.000000]
			*	总市值：marketAmt[0.000000]
			*	总资产：assetAmt[0.000000]
			*/
			printf("请输入账户编号:");
			scanf("%s", str);
			X2Balance pBalance;
			pBalance.accountId = atoi(str);	    //账户编号
			strcpy(pBalance.fundKind, "1");     //资金类型
			strcpy(pBalance.opstation, "PC;IIP=xxx.xxx.xxx.xxx;IPORT=xxxxx;LIP=xxx.xxx.xxx.xxx;MAC=xxxxxxxxxxxx;HD=xxxxxxxxxxxx;PCN=xxxxxxxxxxxx;CPU=xxxxxxxxxxxx;PI=xxxxxxxxxxxx;VOL=xxxxxxxxxxxx");	    //站点信息
			p->Balance(&pBalance);              // case3:账户资金查询
			break;
		}
		case 4:{ // 4.股票持仓查询
			/*
			* 股票持仓查询应答
			*	账户编号：accountId[104000001]
			*	资金账户：accountUser[202304181002]
			*	证券代码：symbol[002239]
			*	市场：marketType[1]
			*	期初数量：openQty[0]
			*	持有数量：holdQty[1500]
			*	可用数量：availQty[0]
			*	最新价：lastPx[0.000000]
			*	成本价：costPx[2.540100]
			*	盈亏余额：profitAmt[-38102998.000000]
			*	定位串：queryPos[0]
			*/
			printf("请输入账户编号:");
			scanf("%s", str);
			X2StockPosition pPosition;
			pPosition.accountId = atoi(str);	//账户编号;
			//pPosition.symbol;	                //证券代码;
			strcpy(pPosition.queryPos, "");	    //定位串（默认初始为空）;
			pPosition.queryNum = 2000;	        //单次最大数量;
			//pPosition.opstation;	            //站点信息;
			p->StockPosition(&pPosition);
			break;
		}
		case 5:{ // 5.回报订阅
			/*
			订阅主题
				账户编号：accountId[104000001]
				订阅类型：type[1]
				20230509 12:31:50.897000 [Subscribe]: accountId[104000001] type[1], send success.
				20230509 12:31:50.898000 [Subscribe]: accountId[104000001] type[2], send success.
				20230509 12:31:50.898000 [Subscribe]: accountId[104000001] type[3], send success.
				20230509 12:31:50.925000 [OnSubTopic]:success
				20230509 12:31:50.953000 [OnSubTopic]:success
				20230509 12:31:51.024000 [OnSubTopic]:success
			*/
			printf("请输入账户编号:");
			scanf("%s", str);
			X2Topic topic;
			topic.accountId = atoi(str);	//账户编号
			topic.type = ReportType::REPORT_TYPE_DEAL;	         //订阅成交
			p->Subscribe(&topic);

			X2Topic topic2;
			topic2.accountId = atoi(str);	//账户编号
			topic2.type = ReportType::REPORT_TYPE_ORDER;	     //订阅委托
			p->Subscribe(&topic2);

			X2Topic topic3;
			topic3.accountId = atoi(str);	//账户编号
			topic3.type = ReportType::REPORT_TYPE_ALGOORDER;	 //订阅母单
			p->Subscribe(&topic3);
			break;
		}
		case 6:{ // 6.取消订阅
			printf("请输入账户编号:");
			scanf("%s", str);
			X2Topic topic;
			topic.accountId = atoi(str);	//账户编号
			topic.type = ReportType::REPORT_TYPE_DEAL;	       //订阅成交
			p->UnSubscribe(&topic);

			X2Topic topic2;
			topic2.accountId = atoi(str);	//账户编号
			topic2.type = ReportType::REPORT_TYPE_ORDER;	    //订阅委托
			p->UnSubscribe(&topic2);

			X2Topic topic3;
			topic3.accountId = atoi(str);	//账户编号
			topic3.type = ReportType::REPORT_TYPE_ALGOORDER;	//订阅母单
			p->UnSubscribe(&topic3);
			break;
		}
		case 7:{ // 7.子单订单查询
			/*
				子单成交查询应答
					交易员编号:traderId[10400001]
					资金账号:accountUser[202304181002]
					账户编号:accountId[104000001]
					算法编号:algoId[101]
					券商委托编号:brokerOrderId[KF_6600504346760000600]
					系统内母单编号:sysQuoteId[6880504346260000400]
					系统内子单编号:sysOrderId[6600504346760000600]
					订单类型:orderType[1]
					市场类型:marketType[2]
					开平标志:openClose[0]
					方向:Side[2]
					证券代码:symbol[600675]
					委托价格:orderPx[2.810000]
					委托数量:orderQty[300]
					发生时间:transacTime[93756090]
					委托状态:orderStatus[4]
					成交数量:cumQty[300]
					成交金额:cumAmt[843.000000]
					剩余数量:leavesQty[0]
					撤单时间:cancelTime[0]
					更新时间:updateTime[93814109]
					备注:note[]
					定位串:queryPos[0]
			*/
			X2OrderActualQuery pOrderActual;
			printf("请输入账户编号:");
			scanf("%s", str);
			pOrderActual.accountId = atoi(str);	//账户编号
			//strcpy(pOrderActual.symbol, "600000");	//证券代码
			//pOrderActual.marketType = AtxMarketType::MARKET_TYPE_SH; //市场
			//pOrderActual.sysOrderId;	//系统委托编号
			printf("请输入查询类型:0-全部委托 1-可撤委托");
			scanf("%s", str);
			pOrderActual.cancelFlag = atoi(str);	//0 - 全部委托，1 - 可撤委托
			strcpy(pOrderActual.queryPos, "0");	//定位串（默认初始为空）
			pOrderActual.queryNum = 100;	//单次最大数量（默认最大2000）
			//pOrderActual.opstation;	//站点信息
			p->QueryOrderActual(&pOrderActual);
			break;
		}
		case 8:{ // 8.母单订单查询
			/*
				交易员编号:traderId[10400001]
				算法编号:algoId[101]
				算法类型编号:algoTypeId[101]
				算法类型名称:algoTypeName[TWAP]
				机构编号:orgId[0]
				账户编号:accountId[104000001]
				资金账号:accountUser[202304181002]
				证券代码:symbol[000612]
				市场类型:marketType[1]
				开始时间:effectiveTime[20230509091659000]
				结束时间:expireTime[20230509092500000]
				系统内母单编号:sysQuoteId[6880504334190000100]
				发生时间:transacTime[91659444]
				自定义参数:algoParam[{"after_action":0,"limit_action":0,"organ_id":1040,"price":null,"side":2,"task_qty":2200.0}]
				任务数量:taskQty[2200]
				成交数量1:cumQty1[0]
				成交数量2(TO卖有用):cumQty2[0]
				在途数量1:outstandingQty1[0]
				在途数量2(TO卖有用):outstandingQty2[0]
				剩余数量1: leavesQty1[0]
				剩余数量2(TO卖有用):leavesQty2[0]
				任务金额:taskAmt[11264.000000]
				成交金额1:cumAmt1[0.000000]
				成交金额2(TO卖有用):cumAmt2[0.000000]
				备注:note[Time is up]
				更新时间:updateTime[93000190]
				委托状态:orderStatus[10]
				篮子编号:basketId[]
				三方编号:clQuoteId[]
				定位串:queryPos[0]                            
			*/
			X2ClientInstructionQuery pOrderInstruction;
			printf("请输入账户编号:");
			scanf("%s", str);
			pOrderInstruction.accountId = atoi(str);	//账户编号
			printf("请输入查询类型:1-所有母单 2-非终止状态母单");
			scanf("%s", str);
			pOrderInstruction.queryType = atoi(str);	//1 - 所有母单2 - 非终止状态母单
			pOrderInstruction.queryPos = 0;	//定位串（默认初始为0）
			pOrderInstruction.queryNum = 10;	//单次最大数量（默认最大2000）
			p->QueryOrderInstruction(&pOrderInstruction);
			break;
		}
		case 9:{ // 9.算法类型查询
			/*
			算法类型查询应答：
				交易员编号：traderId[10400001]
				账户编号：accountId[104000001]
				资金账号：accountUser[202304181002]
				算法类型编号：  algoTypeId[106、   102、          201、       101、        101、          102、          105]
				算法类型名称：algoTypeName[T0、    VWAP、       PASSTHRU、   TWAP、       TWAP、          VWAP、         POV]
				算法编号：         algoId[106、    102、          201、      101、        103、           104、          105]
				算法名称：       algoName[kf_t0、kf_vwap_plus、kf_passthru、kf_twap_plus、kf_twap_core、kf_vwap_core、kf_pov_core]

				算法：102、VWAP、102、kf_vwap_plus

				机构编号：orgId[0]
				算法类型字段信息：algoTypeFieldsInfo

			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[106] algoTypeName[T0] algoId[106] algoName[kf_t0] orgId[0] algoTypeFieldsInfo[{"buyside":{"name":"买入方向","type":"Enum","required":"1","enum_type":{"普通买入":"1","融资买入":"5"}},"sellside":{"name":"卖出方向","type":"Enum","required":"1","enum_type":{"普通卖出":"2","融券卖出":"4"}},"task_qty":{"name":"任务数量","type":"Int","required":"1", "max":"9999999", "min" :"1", "multiplier":"1"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[102] algoTypeName[VWAP] algoId[102] algoName[kf_vwap_plus] orgId[0] algoTypeFieldsInfo[{"side":{"name":"交易方向","type":"Enum","required":"1", "enum_type" : {"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1", "max":"99999999", "min" :"1", "multiplier":"1"}, "price": {"name":"限价","type":"Float","required":"0","precision":"3"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[201] algoTypeName[PASSTHRU] algoId[201] algoName[kf_passthru] orgId[0] algoTypeFieldsInfo[{"PriceTypeI":{"name":"报价类型","type":"Enum","required":"1","enum_type":{"限价":"0","卖五价":"1","卖四价":"2","卖三价":"3","卖二价":"4","卖一价":"5","最新价":"6","买一价":"7","买二价":"8","买三价":"9","买四价":"10","买五价":"11"}},"price":{"name":"价格","type":"Float","required":"0","required_ref":"PriceTypeI=0","precision":"3"},"side":{"name":"交易方向","type":"Enum","required":"1","enum_type":{"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1","max":"9999999","min":"1","multiplier":"1"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[101] algoTypeName[TWAP] algoId[101] algoName[kf_twap_plus] orgId[0] algoTypeFieldsInfo[{"side":{"name":"交易方向","type":"Enum","required":"1", "enum_type" : {"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1", "max":"99999999", "min" :"1", "multiplier":"1"}, "price": {"name":"限价","type":"Float","required":"0","precision":"3"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[101] algoTypeName[TWAP] algoId[103] algoName[kf_twap_core] orgId[0] algoTypeFieldsInfo[{"side":{"name":"交易方向","type":"Enum","required":"1", "enum_type" : {"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1", "max":"99999999", "min" :"1", "multiplier":"1"}, "price": {"name":"限价","type":"Float","required":"0","precision":"3"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[102] algoTypeName[VWAP] algoId[104] algoName[kf_vwap_core] orgId[0] algoTypeFieldsInfo[{"side":{"name":"交易方向","type":"Enum","required":"1", "enum_type" : {"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1", "max":"99999999", "min" :"1", "multiplier":"1"}, "price": {"name":"限价","type":"Float","required":"0","precision":"3"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]
			[OnRspQueryAlgoType]:traderId[10400001] accountId[104000001] accountUser[202304181002] algoTypeId[105] algoTypeName[POV] algoId[105] algoName[kf_pov_core] orgId[0] algoTypeFieldsInfo[{"side":{"name":"交易方向","type":"Enum","required":"1","enum_type":{"普通买入":"1","普通卖出":"2","买券还券":"3","融券卖出":"4","融资买入":"5","卖券还款":"6"}},"task_qty":{"name":"任务数量","type":"Int","required":"1"},"max_percentage":{"name":"最大市场参与比例%","type":"Float","required":"1","max":"30","min":"0","precision":"0","multiplier":"1","default":"30"},"price":{"name":"限价","type":"Float","required":"0","precision":"3"},"limit_action":{"name":"涨跌停是否继续执行","type":"Enum","required":"1","enum_type":{"涨停不卖跌停不买":"0","涨跌停继续交易":"1","涨跌停终止交易":"2","涨停不买跌停不卖":"3"},"default":"0"},"after_action":{"name":"过期后是否继续执行","type":"Bool","required":"1","default":"0"}}]

			*/
			printf("请输入账户编号:");
			scanf("%s", str);
			X2ClientQueryAtxInfo pAlgo;
			pAlgo.accountId = atoi(str);
			p->QueryAlgoType(&pAlgo);
			break;
		}
		case 10:{ // 10.母单下单请求
			/*
				母单下单应答:
					三方编号：sysQuoteId[6880504541840000100]
					系统内母单编号：clQuoteId[202350915006031332]
					篮子编号：basketId[202350915006031332]
			*/
			// p->ReqInstructionCreate();

			/*
			 算法类型编号:algoTypeId[102]
				机构编号:orgId[0]
				账户编号:accountId[104000001]
				证券代码:symbol[000002]
				市场类型:marketType[11]
				开始时间:effectiveTime[20230509091500000]
				结束时间:expireTime[20230509092500000]
				三方编号:clQuoteId[202350915218905425]
				自定义参数:algoParam[{"side":1,"price":7,"task_qty":1000,"limit_action":0,"after_action":0}]
				算法编号:algoId[102]
				篮子编号:basketId[202350915218905425]
				站点信息:opstation
			*/
			cout << "进入主动下单算法请求++++++++++++++++++++++++++++++" << endl;

			// 下单不变的参数
			char const* t_algoTypeId = "102";                   // 算法类型编号
			char const* t_orgId = "0";                          // 机构编号
			char const* t_accountId = "104000001";              // 账户编号 * —————— 对应jltest1账户
			// char const* t_accountId = "104000002";              // 账户编号 * —————— 对应jltest2账户

			char const* t_clQuoteId = " ";  // 可省略-三方编号
			char const* t_algoId = "102";   // 算法编号
			char const* t_basketId = " ";   // 可省略-篮子编号
			char const* t_opstation = " ";  // 可省略-站点信息

			// 一、卖出下单
			for (i = 0; i < SellInfoArray.size(); i++)
			{
				Scode = SellInfoArray[i][0];        // 卖出股票代码  *
				//Sv2 = SellInfoArray[i][1];        // 市场
				Svolume = SellInfoArray[i][2];      // 卖出数量      *
				//Sweight = SellInfoArray[i][3];    // 卖出相对权重
				Sside = SellInfoArray[i][4];        // 市场类型       *

				// 1.1 下单改变的参数
				string t_sell_symbol = getStockCode(Scode);              // 证券代码
				cout << t_sell_symbol << " " << Svolume << " " << Sside << endl;

				// side：买卖方向: 买入为1，卖出为2
				// task_qty：委托股数
				// limit_action：0-涨跌停不继续交易、1-涨跌停继续交易
				// after_action：0-过期后不交易、1-过期后继续交易

				string s1 = "{\"side\":2,\"task_qty\":";
				s1 += Svolume;
				string s2 = ",\"limit_action\":0,\"after_action\":1}";   
				s1 += s2;                                                // 下卖单参数
				char const* t_algoParam = s1.c_str();
				char const* t_marketType = Sside.c_str();                // 市场类型

				p->ReqInstructionCreateNo2(
					t_algoTypeId,
					t_orgId,
					t_accountId,
					t_sell_symbol,
					t_marketType,
					t_effectiveTime,
					t_expireTime,
					t_clQuoteId,
					t_algoParam,
					t_algoId,
					t_basketId,
					t_opstation
				);
			}

			// 二、买入下单
			for (m = 0; m < BuyInfoArray.size(); m++)
			{
				Bcode = BuyInfoArray[m][0];       // 买入股票代码
				//Bv2   = BuyInfoArray[m][1];     // 市场
				Bvolume = BuyInfoArray[m][2];     // 买入数量
				//Bweight = BuyInfoArray[m][3];   // 买入相对权重
				Bside = BuyInfoArray[m][4];       // 市场类型

				// 下单改变的参数
				// side：买卖方向: 买入为1，卖出为2
				// task_qty：委托股数
				// limit_action：0-涨跌停不继续交易、1-涨跌停继续交易
				// after_action：0-过期后不交易、1-过期后继续交易

				string t_buy_symbol = getStockCode(Bcode);        // 证券代码
				cout << t_buy_symbol << " " << Bvolume << " " << Bside << endl;

				string s1 = "{\"side\":1,\"task_qty\":";
				s1 += Bvolume;
				string s2 = ",\"limit_action\":0,\"after_action\":1}";
				s1 += s2;
				char const* t_algoParam = s1.c_str();             // 下买单参数

				char const* t_marketType = Bside.c_str();         // 市场类型

				p->ReqInstructionCreateNo2(
					t_algoTypeId,
					t_orgId,
					t_accountId,
					t_buy_symbol,
					t_marketType,
					t_effectiveTime,
					t_expireTime,
					t_clQuoteId,
					t_algoParam,
					t_algoId,
					t_basketId,
					t_opstation
				);
			}

			break;
		}
		case 11:{ // 11.母单撤单请求
			printf("请输入系统内母单编号:");
			scanf("%s", str);
			X2ClientInstructionCancel pInstructionCancel;
			pInstructionCancel.sysQuoteId = atoll(str);	//系统内母单编号
			pInstructionCancel.cancelType = AtxCancelInstructionType::CANCEL_INSTRUCTION_STOPTRADE;	//撤单类型
			p->InstructionCancel(&pInstructionCancel, 1);
			break;
		}
		case 12:{ // 12.母单改单请求
			X2ClientInstructionReplace pInstructionReplace;
			printf("请输入系统内母单编号:");
			scanf("%s", str);
			pInstructionReplace.sysQuoteId = atoll(str);	//系统内母单编号
			printf("请输入开始时间:");
			scanf("%s", str);
			pInstructionReplace.effectiveTime = atoll(str);	//开始时间
			printf("请输入结束时间:");
			scanf("%s", str);
			pInstructionReplace.expireTime = atoll(str);	//结束时间
			std::string tmp = "{\"side\":1,\"price\":7,\"task_qty\":1000,\"limit_action\":0,\"after_action\":0}";
			strcpy(pInstructionReplace.algoParam, tmp.c_str());	//自定义参数
			p->InstructionReplace(&pInstructionReplace);
			break;
		}
		case 13:{ // 13.母单暂停请求
			printf("请输入系统内母单编号:");
			scanf("%s", str);
			X2ClientInstructionControl pInstructionControl;
			pInstructionControl.sysQuoteId = atoll(str);	//系统内母单编号
			pInstructionControl.controlType = 0;	//0 - 暂停，1 - 恢复
			p->InstructionControl(&pInstructionControl);
			break;
		}
		case 14:{ // 14.母单恢复请求
			printf("请输入系统内母单编号:");
			scanf("%s", str);
			X2ClientInstructionControl pInstructionControl;
			pInstructionControl.sysQuoteId = atoll(str);	//系统内母单编号
			pInstructionControl.controlType = 1;	//0 - 暂停，1 - 恢复
			p->InstructionControl(&pInstructionControl);
			break;
		}
		case 15:{ // 15.修改资金账号密码
			X2AccountChangePwdReq pAccountChangePwd;
			//pAccountChangePwd.traderId = traderId;
			printf("请输入账户编号:");
			scanf("%s", str);
			pAccountChangePwd.accountId = atoi(str);
			printf("请输入交易员新密码:");
			scanf("%s", str);
			strcpy(pAccountChangePwd.newpassword, str);
			p->AccountChangePwdReq(&pAccountChangePwd);
			break;
		}
		case 16:{ // 16.通过资金资产账户查询资金账号
			printf("请输入资金账号:");
			scanf("%s", str);
			X2QueryAccountIdReq pQueryAccountId;
			strcpy(pQueryAccountId.accountUser, str);
			p->QueryAccountId(&pQueryAccountId);
			break;
		}
		case 17:{ // 17.结算子单查询
			X2SettleActualQryReq pSettleActualQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleActualQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleActualQry.endDate = atoi(str);
			//定位串(默认初始为0)
			pSettleActualQry.queryPos = 0;
			//单次最大数量(默认最大10000),若返回数量小于该数量说明已查询完成,否则继续查询,定位串使用最后一次响应返回的queryPos
			pSettleActualQry.queryNum = 10000;
			p->SettleActualQry(&pSettleActualQry);
			break;
		}
		case 18:{ // 18.结算母单查询
			X2SettleInstructionQryReq pSettleInstructionQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleInstructionQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleInstructionQry.endDate = atoi(str);
			//定位串(默认初始为0)
			pSettleInstructionQry.queryPos = 0;
			//单次最大数量(默认最大10000),若返回数量小于该数量说明已查询完成,,否则继续查询,定位串使用最后一次响应返回的queryPos
			pSettleInstructionQry.queryNum = 10000;
			p->SettleInstructionQry(&pSettleInstructionQry);
			break;
		}
		case 19:{ // 19.结算汇总查询
			X2SettleSummaryQryReq pSettleSummaryQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleSummaryQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleSummaryQry.endDate = atoi(str);
			p->SettleSummaryQry(&pSettleSummaryQry);
			break;
		}
		case 20:{ // 20.结算篮子汇总查询
			X2SettleBasketSummaryQryReq pSettleBasketSummaryQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleBasketSummaryQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleBasketSummaryQry.endDate = atoi(str);
			p->SettleBasketSummaryQry(&pSettleBasketSummaryQry);
			break;
		}
		case 21:{ // 21.T0策略——(趋势策略)下单请求
			/*
			算法类型编号:algoTypeId[106]
				机构编号:orgId[0]
				账户编号:accountId[104000001]
				证券代码:symbol[000002]
				市场类型:marketType[1]
				开始时间:effectiveTime[20230509091500000]
				结束时间:expireTime[20230509092500000]
				三方编号:clQuoteId[202350915218905425]
				自定义参数:algoParam[{ "buyside": 1, "sellside":2, "task_qty": 16000, "limit_action": 0, "after_action": 0}]
				算法编号:algoId[106]
				篮子编号:basketId[202350915218905425]
				站点信息:opstation
			*/
			cout << "请注意进入T0(趋势策略)下单请求+++++++++++++++++++++++++" << endl;

			// 下单不变的参数
			char const* t_algoTypeId = "106";                   // 算法类型编号 *
			char const* t_orgId = "0";                          // 机构编号 *
			char const* t_accountId = "104000001";              // 账户编号 *  —————— 对应jltest1账户

			char const* t_clQuoteId = " ";  // 可省略-三方编号 *
			char const* t_algoId = "106";   // 算法编号 *
			char const* t_basketId = " ";   // 可省略-篮子编号 *
			char const* t_opstation = " ";  // 可省略-站点信息 *

			// 一、获取T0下单文件数据
			vector< vector<string> > T0InfoArray;
			string T0PathName = "./T0dataSource/T0算法交易持仓" + datestr + ".csv";
			T0InfoArray = readCSV(T0PathName);

			// 二、T0下单执行
			for (i = 0; i < T0InfoArray.size(); i++)
			{
				Tcode = T0InfoArray[i][0];        // 去掉卖单后的持仓股票代码    
				//Tv2 = T0InfoArray[i][1];        // 市场名称
				Tvolume = T0InfoArray[i][2];      // T0持仓数量       
				//Tname = T0InfoArray[i][3];      // 股票名称
				Tside = T0InfoArray[i][4];        // 市场类型        *

				// 1.1 下单改变的参数
				string t_sell_symbol = getStockCode(Tcode);     // 证券代码  *
				cout << t_sell_symbol << " " << Tvolume << " " << Tside << endl;

				// 1.2 T0下单时自定义参数
				/*
					kf_t0 { "buyside": 1, "sellside":2, "task_qty": 16000, "limit_action": 0, "after_action": 0}
					-- buyside： 买入 —— 1:普通买入; 5:融资买入
					-- sellside：卖出 —— 2:普通卖出; 4:融券卖出
					-- task_qty：委托股数 —— 任务数量
					-- limit_action：涨跌停是否继续执行 —— 0-涨停不卖跌停不买、1-涨跌停继续交易、2-涨跌停终止交易、3-涨停不买跌停不卖
					-- after_action：过期后是否继续执行 —— 0-过期后不交易、1-过期后继续交易
				*/

				string s1 = "{\"buyside\": 1, \"sellside\":2,\"task_qty\":";
				s1 += Tvolume;
				string s2 = ",\"limit_action\":0,\"after_action\":0}";
				s1 += s2;                                                // 下单参数 *
				char const* t_algoParam = s1.c_str();

				char const* t_marketType = Tside.c_str();                // 市场类型

				p->ReqInstructionCreateNo2(
					t_algoTypeId,
					t_orgId,
					t_accountId,
					t_sell_symbol,
					t_marketType,
					t0_effectiveTime,   // T0交易下单开始时间
					t0_expireTime,      // T0交易下单结束时间
					t_clQuoteId,
					t_algoParam,
					t_algoId,
					t_basketId,
					t_opstation
				);
			}

			break;
		}
		case 22:{ // 22.结算T0汇总查询
			X2SettleT0SummaryQryReq pSettleT0SummaryQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleT0SummaryQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleT0SummaryQry.endDate = atoi(str);
			p->SettleT0SummaryQry(&pSettleT0SummaryQry);
			break;
		}
		case 23: { // 23.T0策略——(高频策略)下单请求
			/*
			算法类型编号:algoTypeId[106]
				机构编号:orgId[0]
				账户编号:accountId[104000002]
				证券代码:symbol[000002]
				市场类型:marketType[1]
				开始时间:effectiveTime[20230509091500000]
				结束时间:expireTime[20230509092500000]
				三方编号:clQuoteId[202350915218905425]
				自定义参数:algoParam[{ "buyside": 1, "sellside":2, "task_qty": 16000, "limit_action": 0, "after_action": 0}]
				算法编号:algoId[106]
				篮子编号:basketId[202350915218905425]
				站点信息:opstation
			*/
			cout << "请注意进入T0(高频策略)下单请求+++++++++++++++++++++++++" << endl;

			// 下单不变的参数
			char const* t_algoTypeId = "106";                   // 算法类型编号 *
			char const* t_orgId = "0";                          // 机构编号 *
			char const* t_accountId = "104000002";              // 账户编号 * —————— 对应jltest2账户

			char const* t_clQuoteId = " ";  // 可省略-三方编号 *
			char const* t_algoId = "106";   // 算法编号 *
			char const* t_basketId = " ";   // 可省略-篮子编号 *
			char const* t_opstation = " ";  // 可省略-站点信息 *

			// 一、获取T0下单文件数据
			vector< vector<string> > T0InfoArray;
			string T0PathName = "./T0dataSource/T0算法交易持仓" + datestr + ".csv";
			T0InfoArray = readCSV(T0PathName);

			// 二、T0下单执行
			for (i = 0; i < T0InfoArray.size(); i++)
			{
				Tcode = T0InfoArray[i][0];        // 去掉卖单后的持仓股票代码    
				//Tv2 = T0InfoArray[i][1];        // 市场名称
				Tvolume = T0InfoArray[i][2];      // T0持仓数量       
				//Tname = T0InfoArray[i][3];      // 股票名称
				Tside = T0InfoArray[i][4];        // 市场类型        *

				// 1.1 下单改变的参数
				string t_sell_symbol = getStockCode(Tcode);     // 证券代码  *
				cout << t_sell_symbol << " " << Tvolume << " " << Tside << endl;

				// 1.2 T0下单时自定义参数
				/*
					kf_t0 { "buyside": 1, "sellside":2, "task_qty": 16000, "limit_action": 0, "after_action": 0}
					-- buyside： 买入 —— 1:普通买入; 5:融资买入
					-- sellside：卖出 —— 2:普通卖出; 4:融券卖出
					-- task_qty：委托股数 —— 任务数量
					-- limit_action：涨跌停是否继续执行 —— 0-涨停不卖跌停不买、1-涨跌停继续交易、2-涨跌停终止交易、3-涨停不买跌停不卖
					-- after_action：过期后是否继续执行 —— 0-过期后不交易、1-过期后继续交易
				*/

				string s1 = "{\"buyside\": 1, \"sellside\":2,\"task_qty\":";
				s1 += Tvolume;
				string s2 = ",\"limit_action\":0,\"after_action\":0}";
				s1 += s2;                                                // 下单参数 *
				char const* t_algoParam = s1.c_str();

				char const* t_marketType = Tside.c_str();                // 市场类型

				p->ReqInstructionCreateNo2(
					t_algoTypeId,
					t_orgId,
					t_accountId,
					t_sell_symbol,
					t_marketType,
					t0_effectiveTime,   // T0交易下单开始时间
					t0_expireTime,      // T0交易下单结束时间
					t_clQuoteId,
					t_algoParam,
					t_algoId,
					t_basketId,
					t_opstation
				);
			}

			break;
		}
		case 24:{ // 24.结算隔夜仓查询
			X2SettleT0OverNightQryReq pSettleT0OverNightQry;
			printf("请输入开始日期(20220520):");
			scanf("%s", str);
			pSettleT0OverNightQry.beginDate = atoi(str);
			printf("请输入结束日期(20220520):");
			scanf("%s", str);
			pSettleT0OverNightQry.endDate = atoi(str);
			p->SettleT0OverNightQry(&pSettleT0OverNightQry);
			break;
		}
		case 25:{ // 25.子单成交查询
			X2TradeActualQuery pTradeActual;
			p->QueryTradeActual(&pTradeActual);
			break;
		}
		default:
			printf("未知操作，请重新输入\n");
			break;
		}
	}

	return 0;
}
