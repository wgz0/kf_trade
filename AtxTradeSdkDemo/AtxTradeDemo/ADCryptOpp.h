#pragma once

#include <iostream>
#include <sstream>
#include <string>
using namespace std;

#include "aes.h"
#include "modes.h"
#include "filters.h"
using namespace CryptoPP;

class cryptopp
{
public:
    // aes加密相关函数
    static bool aes_init(const string& key, const string& iv);
    static string aes_encrypt(const string& inputPlainText);
    static string aes_decrypt(const string& cipherTextHex);

private:
    // 静态成员变量
    static unsigned char s_aes_key[AES::DEFAULT_KEYLENGTH];
    static unsigned char s_aes_iv[AES::DEFAULT_KEYLENGTH];
};
