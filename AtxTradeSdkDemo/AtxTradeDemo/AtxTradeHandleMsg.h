﻿#pragma once

#ifdef _WIN32
#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <conio.h>
#else
#include <unistd.h>
#include <fcntl.h>
#endif
#include "X2AtxTradeApi.h"
#include "X2AtxTradeSpi.h"
#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>
#include <mutex>
#include <queue>
#include <iostream>
#include <string>
#include <vector>
#include <cctype>
#include <iostream>
#include <string>
#include <sstream>
#include <string.h>
#include <fstream> 
#include <stdlib.h>
#include <cstdio>
#include "toolGeneral.hpp"
#include "string.h"

class HandleMsg : public X2ClientTradeSpi
{
public:
    //建立连接成功后回调
    virtual void OnConnect();
    //连接断开时回调
    virtual void OnDisconect();
	//登录应答
	virtual void OnRspLogin(X2LoginRsp* pRspLogin, X2RspInfoField* pRspInfo, int nRequestID);
	//资金账号查询应答
	virtual void OnRspQueryAccountInfo(X2FundAccountInfoRsp* pRspAccount, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //通过资金资产账户查询资金账号应答
    virtual void OnRspQueryAccountId(X2QueryAccountIdRsp* pRspQueryAccountId, X2RspInfoField* pRspInfo, int nRequestID);
	//资金账号密码修改应答
	virtual void OnRspAccountChangePwd(X2AccountChangePwdRsp* pAccountChangePwd, X2RspInfoField* pRspInfo, int nRequestID);
	//账户资金查询应答
	virtual void OnRspBalance(X2BalanceRsp* pBalance, X2RspInfoField* pRspInfo, int nRequestID);
	//股票持仓查询应答
	virtual void OnRspStockPosition(X2StockPositionRsp* pPosition, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//子单委托回报主推
	virtual void OnRtnOrderReport(X2OrderActualReport* orderReport);
	//订阅主题响应
	virtual void OnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID);
    //取消订阅主题响应
    virtual void OnUnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID);
	//子单成交回报主推
	virtual void OnRtnTradeReport(X2TradeActualReport* tradeReport);
	//母单委托回报主推
	virtual void OnRtnInstructionReport(X2ClientInstructionReport* pReport);
	//子单订单查询应答
	virtual void OnRspQueryOrderActual(X2OrderActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//母单订单查询应答
	virtual void OnRspQueryOrderInstruction(X2ClientInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//算法类型查询应答
	virtual void OnRspQueryAlgoType(X2ClientQueryAtxInfoRsp* pRspAtx, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//母单下单应答
	virtual void OnRspInstructionCreate(X2ClientInstructionCreateRsp* pRspCreate, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//母单撤单应答
	virtual void OnRspInstructionCancel(X2ClientInstructionCancelRsp* pRspCancel, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//母单改单应答
	virtual void OnRspInstructionReplace(X2ClientInstructionReplaceRsp* pRspReplace, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
	//母单暂停/恢复应答
	virtual void OnRspInstructionControl(X2ClientInstructionControlRsp* pRspControl, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算子单查询
    virtual void OnRspSettleActualQry(X2SettleActualQryRsp* pSettleActualQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算母单查询
    virtual void OnRspSettleInstructionQry(X2SettleInstructionQryRsp* pSettleSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算汇总查询
    virtual void OnRspSettleSummaryQry(X2SettleSummaryQryRsp* pSettleInstructionQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算篮子汇总查询
    virtual void OnRspSettleBasketSummaryQry(X2SettleBasketSummaryQryRsp* pSettleBasketSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算T0汇总查询
    virtual void OnRspSettleT0SummaryQry(X2SettleT0SummaryQryRsp* pSettleT0SummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //结算隔夜仓查询
    virtual void OnRspSettleT0OverNightQry(X2SettleT0OverNightQryRsp* pSettleT0OverNightQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //子单成交查询应答
    virtual void OnRspQueryDealActual(X2DealActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //母单历史订单查询应答
    virtual void OnRspQueryHistoryOrderInstruction(X2ClientHistoryInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID);
    //一地多户查询应答
    virtual void OnRsQueryAcctIdsByAcctUser(X2AcctIdsByAcctUserQueryRsp* pAcctIdsByAcctUser, int nCount, X2RspInfoField* pRspInfo, int nRequestID);


public:
    HandleMsg();
    ~HandleMsg();

	// 母单下单应答接口01
    void ReqInstructionCreate();

	// 母单下单应答接口02(针对主动下单算法)
	void ReqInstructionCreateNo2(
		char const*& algoTypeId,
		char const*& orgId,
		char const*& accountId,
		std::string symbol,
		char const*& marketType,
		char const*& effectiveTime,
		char const*& expireTime,
		char const*& clQuoteId,
		std::string algoParam,
		char const*& algoId,
		char const*& basketId,
		char const*& opstation);

	// 

	void InitHandleMsg(HandleMsg *p);
	//登录
    long long Login(X2LoginReq* p);
	//资金账号查询
	long long QueryAccount();
    //资金账号查询
    long long QueryAccountId(X2QueryAccountIdReq* pQueryAccountId);
	//账户资金查询
	long long Balance(X2Balance* pBalance);
	//股票持仓查询
	long long StockPosition(X2StockPosition* pPosition);
	//回报订阅
	long long Subscribe(X2Topic* topic);
	//取消订阅
	long long UnSubscribe(X2Topic* topic);
	//子单订单查询
	long long QueryOrderActual(X2OrderActualQuery* pOrderActual);
	//母单订单查询
	long long QueryOrderInstruction(X2ClientInstructionQuery* pOrderInstruction);
	//算法类型查询
	long long QueryAlgoType(X2ClientQueryAtxInfo* pAlgo);
	//母单下单请求
	long long InstructionCreate(X2ClientInstructionCreate* pInstructionCreate, int nCount = 1);
	//母单撤单请求
	long long InstructionCancel(X2ClientInstructionCancel* pInstructionCancel, int nCount = 1);
	//母单改单请求
	long long InstructionReplace(X2ClientInstructionReplace* pInstructionReplace, int nCount = 1);
	//母单暂停/恢复请求
	long long InstructionControl(X2ClientInstructionControl* pInstructionControl, int nCount = 1);
	//修改资金账号密码
	long long AccountChangePwdReq(X2AccountChangePwdReq* pAccountChangePwd);
    //结算子单查询
    long long SettleActualQry(X2SettleActualQryReq* pSettleActualQry);
    //结算母单查询
    long long SettleInstructionQry(X2SettleInstructionQryReq* pSettleSummaryQry);
    //结算汇总查询
    long long SettleSummaryQry(X2SettleSummaryQryReq* pSettleInstructionQry);
    //结算篮子汇总查询
    long long SettleBasketSummaryQry(X2SettleBasketSummaryQryReq* pSettleBasketSummaryQry);
    //结算T0汇总查询
    long long SettleT0SummaryQry(X2SettleT0SummaryQryReq* pSettleT0SummaryQry);
    //结算隔夜仓查询
    long long SettleT0OverNightQry(X2SettleT0OverNightQryReq* pSettleT0OverNightQry);
    //子单成交查询
    long long QueryTradeActual(X2TradeActualQuery* pTradeActual);

    int StartDumpData();
    void ThreadDumpData();


public:
	//void SendMsg(HandleMsg *pMsg, int seconds, int symbol);
	std::string traderId;
	std::string password;
	std::string accountId;
    bool m_connect = false;
    bool m_create = false;
    std::thread m_dumpData_th;


private:
	X2ClientTradeApi * pApi;
};