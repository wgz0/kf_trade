#include <xlnt/xlnt.hpp>
#include <iostream>

/*
accountId[2147483647] fundKind[1] opstation[PC;IIP=171.83.24.77;IPORT=63921;LIP=192.168.31.112;MAC=7C70DB9ECCD0;HD=WD-WX31A1431260;PCN=XTZJ-20220301HJ;CPU=BFEBFBFF000806D1;PI=C^NTFS^279G;VOL=3AC1-64C7@Atx;V3.0.1.0]

VWAP: kf_vwap_plus、kf_vwap_core
TWAP: kf_twap_plus、kf_twap_core
POV: kf_pov_core
PASSTHRU: kf_passthru

交易网关在 AtxTradeSdk.ini 中进行配置 server_addr
行情: 61.169.7.60:9002
交易: 211.144.194.160:7701

登录用户名：10400001
密码：a12345
资金账号：202304181002

PC : 终端类型 IIP : 公网IP; IIP = 123.112.154.118 IPORT: 公网IP端口号; IPORT = 50361 LIP: 内网IP; LIP = 192.168.118.107 MAC: MAC地址; MAC = 54EE750B1713^FCF8AE5CBD58
HD : 硬盘序列号; HD = TF655AY91GHRVL PCN : PC终端设备名; PCN = XXJSB - ZHUANGSHANG CPU : CPU序列号; CPU = bfebfbff00040651  PI : 硬盘分区信息; PI = C ^ NTFS ^ 99G VOL : 系统盘卷标号; VOL = 0004 - 2CC4;系统:atx;版本号:V1.0.0.0


*/

void create() {
	xlnt::workbook wb;
	xlnt::worksheet ws = wb.active_sheet();
	ws.cell("A1").value(5);
	ws.cell("B2").value("string data");
	ws.cell("C3").formula("=RAND()");
	ws.merge_cells("C3:C4");
	ws.freeze_panes("B2");
	wb.save("./dataSource/readExcel.xlsx");
}

void read() {
	xlnt::workbook wb;
	wb.load("./dataSource/readExcel.xlsx");
	xlnt::worksheet ws = wb.active_sheet();

	std::cout << ws.cell("A1") << std::endl;
	std::cout << ws.cell("B2") << std::endl;
	std::cout << ws.cell("C3") << std::endl;
}

int main_readExcel()
{
	read();
	return 0;
}