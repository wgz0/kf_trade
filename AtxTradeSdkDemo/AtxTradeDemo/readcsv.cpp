#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

using namespace std;


// 1、写入csv文件
void WriteCSV(const string pathName) {
	ofstream outFile;
	outFile.open(pathName, ios::out); // 打开模式可省略
	outFile << "name" << ',' << "age" << ',' << "hobby" << endl;
	outFile << "Mike" << ',' << 18 << ',' << "paiting" << endl;
	outFile << "Tom" << ',' << 25 << ',' << "football" << endl;
	outFile << "Jack" << ',' << 21 << ',' << "music" << endl;
	outFile.close();
}


// 2、逐行打印csv文件
void PrintCSV(const string pathName) {
	ifstream inFile(pathName, ios::in);
	string lineStr;
	vector<vector<string>> strArray;
	while (getline(inFile, lineStr))
	{
		// 打印整行字符串
		cout << lineStr << endl;
		// 存成二维表结构
		stringstream ss(lineStr);
		string str;
		vector<string> lineArray;
		// 按照逗号分隔
		while (getline(ss, str, ','))
			lineArray.push_back(str);
		strArray.push_back(lineArray);
	}
}


// 3、读取CSV文件
vector<vector<double>> read_gt_file(string filename){
    ifstream inFile(filename, ios::in);
    string lineStr;
    char delim=',';
    vector<vector<double>> matrix;
    if(inFile.fail()){
        cout <<"Read files fail....."<<endl;
        return matrix;
    }
    
    while (getline(inFile, lineStr)) {
        stringstream ss(lineStr);
        string str;
        vector<double> lineArray;

        // cut apart by flag
        while (getline(ss, str, delim))
            lineArray.push_back(std::stod(str));
        matrix.push_back(lineArray);
    }
    return matrix;
}


int main_readcsv()
{
	string pathName = "./dataSource/默认篮子格式买0508.csv";
	PrintCSV(pathName);
	return 0;
}