#!/usr/bin/bash
#svn up ../libs/

mkdir -p ./Build/linux/release
mkdir -p ./Build/linux/debug

cd ./Build/linux/debug
cmake -DCMAKE_BUILD_TYPE=Debug ../../..
make

cd ../release
cmake -DCMAKE_BUILD_TYPE=Release ../../..
make

cd ../../..

mkdir -p ./Output/linux/debug/
mkdir -p ./Output/linux/release/

#svn add . --no-ignore --force
#svn commit -m "jenkins build auto commit"
