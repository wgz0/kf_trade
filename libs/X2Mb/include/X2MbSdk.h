/**
 * @copyright	: 上海卡方信息科技有限公司
 * @author		: hxx
 * @brief		: 
 */

#ifndef _X2MB_SDK_H__
#define _X2MB_SDK_H__

namespace X2MB
{
    enum X2mbClientOption
    {
        CLIENT_OPTION_SERVER_HOST = 0,        //服务端ip地址
        CLIENT_OPTION_SERVER_PORT,            //服务端端口
        CLIENT_OPTION_COMM_COMPRESS,          //通讯报文是否压缩
        CLIENT_OPTION_COMM_ENCRYPT,           //通讯报文是否加密
        CLIENT_OPTION_AUTO_RECONNECT,
        CLIENT_OPTION_HEART_BEAT
    };

    enum X2MBMsgType
    {
        X2MB_PACKTYPE_REQUEST = 0,
        X2MB_PACKTYPE_ANSWER = 1,
        X2MB_PACKTYPE_PUBLISH = 9
    };

    enum X2mbProtocolType
    {
        X2_PROTOCAL_PB = 0,
        X2_PROTOCAL_STRUCT = 1,
        X2_PROTOCAL_FIX = 2
    };

    class IX2MBMsg
    {
    public:

        /**
        * @brief    :  设置功能号
        * @param    :  const char* url 功能号url字符串
        * @return   :  int
        **/
        virtual int  SetFnUrl(const char* url) = 0;

        /**
        * @brief    :  获取功能号
        * @return   :  const char*
        **/
        virtual const char* GetFnUrl() = 0;

        /**
        * @brief    :  设置目标topic
        * @param    :  const char* topic 订阅回报主题字符串
        * @return   :  int
        * @retval   :  返回0 成功, -1 失败
        **/
        virtual int SetTopic(const char* topic) = 0;

        /**
        * @brief    :  获取目标topic
        * @return   :  const char*
        * @retval   :  返回目标url
        **/
        virtual const char* GetTopic() = 0;

        /**
        * @brief    :  设置业务数据
        * @param    :  const void* buff 数据指针，内部拷贝数据
        * @param    :  int length 数据大小
        **/
        virtual int SetPackData(const void* buff, int length) = 0;

        /**
        * @brief    :   获取包的数据
        * @param    :   pLength 返回业务数据长度
        * @return   :   const void*
        **/
        virtual void* GetPackData(int* pLength = nullptr) = 0;

        /**
        * @brief    :  获取业务数据包的长度
        * @return   :  int
        **/
        virtual int  GetPackLen() = 0;

        /**
        * @brief    :  设置包的类型
        * @param    :  int value 包类型,见 @ X2MBMsgType
        **/
        virtual int  SetPackType(int value) = 0;

        /**
        * @brief    :  获取包的的类型
        * @return   :  int
        **/
        virtual int GetPackType() = 0;


        /**
        * @brief    :  设置消息id
        * @param    :  long index id
        * @return   :  long long
        **/
        virtual long  SetPacketId(long long packetId) = 0;

        /**
        * @brief    :  获取包id
        * @return   :  long long
        **/
        virtual long long GetPacketId() = 0;



        /**
        * @brief    :  设置服务
        * @fuction  :  IX2Message::SetService
        * @return   :  int
        **/
        virtual int SetService(const char* service) = 0;

        /**
        * @brief    :  获取服务
        * @fuction  :  IX2Message::GetService
        * @return   :  const char*
        **/
        virtual const char* GetService() = 0;


        /**
        * @brief    :  获取远程调用的返回值
        * @return   :  int
        **/
        virtual int GetReturnCode() = 0;

        //token
        /**
        * @brief    :  设置token
        * @param    :  const char* url token值
        * @return   :  int
        **/
        virtual int SetToken(const char* token) = 0;

        /**
        * @brief    :  返回token
        * @return   :  const char*
        **/
        virtual const char* GetToken() = 0;

        /**
        * @brief	: 设置 refValue
        * @param	:
        * @return	:
        */
        virtual int SetRefValue(const char* refValue) = 0;

       /**
        * @brief	: 获取 refValue
        * @param	:
        * @return	:
        */
        virtual const char* GetRefValue() = 0;

        /**
        * @brief	: 设置 returnCode
        * @param	:
        * @return	:
        */
        virtual int SetReturnCode(int retCode) = 0;

        /**
        * @brief	: 请求包切换应答包
        * @param	:
        * @return	:
        */
        virtual int ChangeRequest2Answer() = 0;

        /**
        * @brief    :   分配业务包体内存
        * @param    :   length 业务数据长度
        * @return   :   const void*
        **/
        virtual void* AllocPackData(int length) = 0;

        /**
        * @brief	: 设置包的协议
        * @param	: int 包协议类型见 X2mbProtocolType
        * @return	:
        */
        virtual int SetProtocol(int protocol) = 0;

        /**
        * @brief	: 获取包的协议
        * @param	:
        * @return	: int 包协议类型见 X2mbProtocolType
        */
        virtual int GetProtocol() = 0;
    };

    class IX2MbCallback
    {
    public:

        /**
        * @brief	: 建立连接成功后回调
        * @param	:
        * @return	:
        */
        virtual void OnConnect() = 0;


        /**
        * @brief	: 连接断开时回调
        * @param	:
        * @return	:
        */
        virtual void OnDisconect() = 0;


        /**
        * @brief	: 主推回调
        * @param	:
        * @return	:
        */
        virtual void OnPush(IX2MBMsg* pMsg) = 0;


        /**
        * @brief	: 消息回调
        * @desc     : 注意：不要在消息回调中调用 SendMsg()
        * @param	:
        * @return	:
        */
        virtual void OnMsg(IX2MBMsg* pMsg) = 0;
    };

    class IX2MbClient
    {
    public:

        virtual int SetOption(X2mbClientOption option, const char* confValue) = 0;

        /**
        * @brief	: 初始化Client
        * @param	: configFile 
        * @return	:
        */
        virtual int Init(IX2MbCallback* pCallback, const char* configFile = nullptr) = 0;


        /**
        * @brief	: 连接服务端
        * @param	:
        * @return	:
        */
        virtual int Connect() = 0;

        /**
        * @brief	: 同步发送请求
        * @param	: pResponse 应答消息，若超时，pResponse返回nullptr。
        * @return	:
        * @remark   : pRequest, pResponse使用完后，需要调用ReleaseMessage进行释放
        */
        virtual int SendMsg(IX2MBMsg* pRequest, IX2MBMsg** pResponse, int timeoutMs = 5000) = 0;


        /**
        * @brief	: 异步发送请求
        * @param	:
        * @return	:
        */
        virtual int SendMsgAsync(IX2MBMsg* pRequest) = 0;


        /**
        * @brief	: 申请消息
        * @param	:
        * @return	:
        */
        virtual IX2MBMsg* CreateMessage() = 0;
        virtual IX2MBMsg* CreateMessage(const char* service, const char* fnUrl) = 0;


        /**
        * @brief	: 释放消息
        * @param	:
        * @return	:
        */
        virtual void ReleaseMessage(IX2MBMsg* pMsg) = 0;

        /**
        * @brief	: 关闭连接
        * @param	:
        * @return	:
        */
        virtual void DisConnect() = 0;


        /**
        * @brief	: 取最近一次错误信息(线程间错误号相互独立，彼此不影响)
        * @param	:
        * @return	:
        */
        virtual int GetLastError() = 0;


        /**
        * @brief	: 取最近一次错误信息(线程间错误号相互独立，彼此不影响)
        * @param	:
        * @return	:
        */
        virtual const char* GetLastErrorInfo() = 0;


        /**
        * @brief	: 通过错误号取错误信息
        * @param	:
        * @return	:
        */
        virtual const char* GetErrorInfo(int err) = 0;

        /**
        * @brief	: 克隆消息
        * @param	: keepPackData 是否克隆包体，默认 false
        * @return	:
        */
        virtual IX2MBMsg* CreateMessage(IX2MBMsg* pMsg, bool keepPackData = false) = 0;
    };

    IX2MbClient* CreateX2MbClient();

    void DestroyX2MbClient(IX2MbClient* pClient);

    const char* GetX2MbSdkVersion();

}// namespace !X2MB


#endif // !_X2MB_SDK_H__

 
