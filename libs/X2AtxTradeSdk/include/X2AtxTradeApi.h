﻿#pragma once

#include "X2AtxStruct.h"
#include "X2AtxTradeSpi.h"

class X2ClientTradeApi
{
public:
#ifdef _WIN32
    static X2ClientTradeApi* CreateTradeApi(char* profilepath = NULL);
#else
	static __attribute__((visibility("default"))) X2ClientTradeApi* CreateTradeApi(char* profilepath = NULL);
#endif
	virtual void release() = 0;
	virtual bool Connect() = 0;
	virtual bool Disconect() = 0;
	virtual int RegisterSpi(X2ClientTradeSpi* spi) = 0;
	virtual long long Login(X2LoginReq* pLoginInfo, int nRequestID) = 0;
	virtual long long ReqQueryAccountInfo(int nRequestID) = 0;
	virtual long long ReqQueryAccountId(X2QueryAccountIdReq* pQueryAccountId,int nRequestID) = 0;
	virtual long long ReqAccountChangePwd(X2AccountChangePwdReq* pAccountChangePwd, int nRequestID) = 0;
	virtual long long ReqBalance(X2Balance* pBalance, int nRequestID) = 0;
	virtual long long ReqStockPosition(X2StockPosition* pPosition, int nRequestID) = 0;
	virtual long long SubscribeReport(X2Topic* topic, int nRequestID) = 0;
	virtual long long UnSubscribeReport(X2Topic* topic, int nRequestID) = 0;
	virtual long long ReqQueryOrderActual(X2OrderActualQuery* pOrderActual, int nRequestID) = 0;
	virtual long long ReqQueryOrderInstruction(X2ClientInstructionQuery* pOrderInstruction, int nRequestID) = 0;
	virtual long long ReqQueryAlgoType(X2ClientQueryAtxInfo* pAtx, int nRequestID) = 0;
	virtual long long ReqInstructionCreate(X2ClientInstructionCreate* pInstructionCreate, int nRequestID, int nCount = 1) = 0;
	virtual long long ReqInstructionCancel(X2ClientInstructionCancel* pInstructionCancel, int nRequestID, int nCount = 1) = 0;
	virtual long long ReqInstructionReplace(X2ClientInstructionReplace* pInstructionReplace, int nRequestID, int nCount = 1) = 0;
	virtual long long ReqInstructionControl(X2ClientInstructionControl* pInstructionControl, int nRequestID, int nCount = 1) = 0;
	virtual long long ReqSettleActualQry(X2SettleActualQryReq* pSettleActualQry, int nRequestID) = 0;
	virtual long long ReqSettleInstructionQry(X2SettleInstructionQryReq* pSettleSummaryQry, int nRequestID) = 0;
	virtual long long ReqSettleSummaryQry(X2SettleSummaryQryReq* pSettleInstructionQry, int nRequestID) = 0;
	virtual long long ReqSettleBasketSummaryQry(X2SettleBasketSummaryQryReq* pSettleBasketSummaryQry, int nRequestID) = 0;
	virtual long long ReqSettleT0SummaryQry(X2SettleT0SummaryQryReq* pSettleT0SummaryQry, int nRequestID) = 0;
	virtual long long ReqSettleT0OverNightQry(X2SettleT0OverNightQryReq* pSettleT0OverNightQry, int nRequestID) = 0;
	virtual long long ReqQueryTradeActual(X2TradeActualQuery* pTradeActual, int nRequestID) = 0;
	virtual long long ReqQueryHistoryOrderInstruction(X2ClientHistoryInstructionQuery* pHistoryOrderInstruction, int nRequestID) = 0;
	virtual long long ReqQueryAcctIdsByAcctUser(X2AcctIdsByAcctUserQuery* pHistoryOrderInstruction, int nRequestID) = 0;
	virtual ~X2ClientTradeApi();
};

