﻿#pragma once
#include <string.h>
#include "AtxEnum.h"

struct X2LoginReq
{
    int traderId; //交易员编号
    char password[ATX_LENGTH_64]; //交易员密码
    char opstation[ATX_LENGTH_256]; //站点信息
	X2LoginReq():traderId(0)
	{
		memset(password, 0, ATX_LENGTH_64);   // memset是一个初始化函数，作用是将某一块内存中的全部设置为指定的值。
		memset(opstation, 0, ATX_LENGTH_256);
	}
};

struct X2LoginRsp
{
	int traderId;   //交易员编号
	char traderName[ATX_LENGTH_64]; //交易员名称
	int organId;	//机构编号
	AtxTraderStatus traderStatus;	//交易员状态
	X2LoginRsp():traderId(0), organId(0)
	{
		memset(traderName, 0, ATX_LENGTH_64);
		traderStatus = AtxTraderStatus::TRADER_STATUS_INVALID;
	}
};

struct X2FundAccountInfoRsp
{
	int accountId;	//账户编号
	char accountUser[ATX_LENGTH_64];	//资金账号
	char accountName[ATX_LENGTH_64];	//账号名称
	AtxAccountType accountType;	//账户类型
	AtxAssetProp assetProp;	//资产属性
	int accountStatus;	//账户状态
	X2FundAccountInfoRsp():accountId(0), accountStatus(0)
	{
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(accountName, 0, ATX_LENGTH_64);
		accountType = AtxAccountType::ACCOUNT_TYPE_INVALID;
		assetProp = AtxAssetProp::ASSET_PROP_INVALID;
	}
};

struct X2Balance
{
	int accountId;	//账户编号
	char fundKind[ATX_LENGTH_64];	//资金类型
	char opstation[ATX_LENGTH_256];	//站点信息
	X2Balance():accountId(0)
	{
		memset(fundKind, 0, ATX_LENGTH_64);
		memset(opstation, 0, ATX_LENGTH_256);
	}
};

struct X2QueryAccountIdReq
{
    char accountUser[ATX_LENGTH_64];	//资金账号
    X2QueryAccountIdReq()
    {
        memset(accountUser, 0, ATX_LENGTH_64);
    }
};

struct X2QueryAccountIdRsp
{
    int accountId;	//账户编号
    X2QueryAccountIdRsp():accountId(0)
    {
    }
};

struct X2AccountChangePwdReq
{
	int traderId; //交易员编号
	int accountId; //账户编号
	char newpassword[ATX_LENGTH_64]; //交易员密码
	char opstation[ATX_LENGTH_256]; //站点信息
	X2AccountChangePwdReq() :traderId(0), accountId(0)
	{
		memset(newpassword, 0, ATX_LENGTH_64);
		memset(opstation, 0, ATX_LENGTH_64);
	}
};

struct X2AccountChangePwdRsp
{
	int traderId; //交易员编号
	int accountId; //账户编号
	X2AccountChangePwdRsp() :traderId(0), accountId(0)
	{
	}
};

struct X2BalanceRsp
{
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64];	//资金账号
	char fundKind[ATX_LENGTH_64];	//资金类型
	double	availAmt;	//可用资金
	double	drawAmt;	//可取资金
	double	openAmt;	//资金余额
	double	marketAmt;	//总市值
	double	assetAmt;	//总资产
	X2BalanceRsp():accountId(0), availAmt(0.0), drawAmt(0.0), openAmt(0.0), marketAmt(0.0), assetAmt(0.0)
	{
        memset(accountUser, 0, ATX_LENGTH_64);
		memset(fundKind, 0, ATX_LENGTH_64);
	}
};

struct X2StockPosition
{
	int accountId;	//账户编号;
	char symbol[ATX_LENGTH_64];	//证券代码;
	char queryPos[ATX_LENGTH_256];	//定位串（默认初始为空）;
	int	queryNum;	//单次最大数量;
	char opstation[ATX_LENGTH_256];	//站点信息;
	X2StockPosition():accountId(0), queryNum(0)
	{
		memset(symbol, 0, ATX_LENGTH_64);
		memset(queryPos, 0, ATX_LENGTH_256);
		memset(opstation, 0, ATX_LENGTH_256);
	}

};

struct X2RegisterAtx
{
    int algoAppId;	//算法应用编号
    char algoName[ATX_LENGTH_64];	//算法名称
    char algoDesc[ATX_LENGTH_256];	//算法描述
	X2RegisterAtx():algoAppId(0)
	{
		memset(algoName, 0, ATX_LENGTH_64);
		memset(algoDesc, 0, ATX_LENGTH_256);
	}
};
struct X2RegisterAtxRsp
{
    int algoId; //算法应用编号
    char algoName[ATX_LENGTH_64]; ////算法名称
	X2RegisterAtxRsp():algoId(0)
	{
		memset(algoName, 0, ATX_LENGTH_64);
	}
};

struct X2StockPositionRsp
{
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64];	//资金账号
	char symbol[ATX_LENGTH_64];	//证券代码
    AtxMarketType marketType;	//市场
	int	openQty;	//期初数量
	int	holdQty;	//持有数量
	int	availQty;	//可用数量
	double	lastPx;	//最新价
	double	costPx;	//成本价
	double profitAmt;	//盈亏余额
	char queryPos[ATX_LENGTH_256];	//定位串
	X2StockPositionRsp():accountId(0),openQty(0),holdQty(0), availQty(0), lastPx(0.0), costPx(0.0), profitAmt(0.0)
	{
        memset(accountUser, 0, ATX_LENGTH_64);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(queryPos, 0, ATX_LENGTH_256);
	}
};

struct X2OrderActualQuery
{
	int accountId;	//账户编号
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxMarketType marketType;	//市场
	long long sysOrderId;	//系统委托编号
	int cancelFlag;	//0 - 全部委托，1 - 可撤委托
	char queryPos[ATX_LENGTH_256];	//定位串（默认初始为空）
	int queryNum;	//单次最大数量（默认最大2000）
	char opstation[ATX_LENGTH_256];	//站点信息
	X2OrderActualQuery():accountId(0), sysOrderId(0),cancelFlag(0),queryNum(0)
	{
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		memset(symbol, 0, ATX_LENGTH_64);
		memset(queryPos, 0, ATX_LENGTH_256);
		memset(opstation, 0, ATX_LENGTH_256);
	}
};

struct X2OrderActualQueryRsp
{
	int traderId;	//交易员编号
	char accountUser[ATX_LENGTH_64];	//资金账号[64]
	int	accountId;	//账户编号
	int	algoId;	//算法编号
	char brokerOrderId[ATX_LENGTH_256];	//券商委托编号
	long long sysQuoteId;	//系统内母单编号
	long long sysOrderId;	//系统内子单编号
	AtxOrderPriceType orderType;	//订单类型
	AtxMarketType marketType;	//市场类型
	AtxPositionEffectType openClose;	//开平标志
	AtxSide side;	//方向
	char symbol[ATX_LENGTH_64];	//证券代码
	double orderPx;	//委托价格
	int	orderQty;	//委托数量
	int transacTime;	//发生时间
	AtxOrderStatus 	orderStatus;	//委托状态
	int	cumQty;	//成交数量
	double cumAmt;	//成交金额
	int	leavesQty;	//剩余数量
	int cancelTime;	//撤单时间
	int	updateTime;	//更新时间
	char note[ATX_LENGTH_256];	//备注
	char queryPos[ATX_LENGTH_256];	//定位串[256]
	X2OrderActualQueryRsp():traderId(0), accountId(0), algoId(0), sysQuoteId(0), sysOrderId(0), orderPx(0.0),
		orderQty(0), transacTime(0), cumQty(0), cumAmt(0.0), leavesQty(0), cancelTime(0), updateTime(0)
	{
		orderType = AtxOrderPriceType::PRICE_TYPE_INVALID;
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		side = AtxSide::SIDE_INVALID;
		orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(brokerOrderId, 0, ATX_LENGTH_256);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(note, 0, ATX_LENGTH_256);
		memset(queryPos, 0, ATX_LENGTH_256);
	}
};

struct X2ClientInstructionQuery
{
	int accountId;	//账户编号
	int	queryType;	//1 - 所有母单2 - 非终止状态母单
	int	queryPos;	//定位串（默认初始为0）
	int	queryNum;	//单次最大数量（默认最大2000）
	X2ClientInstructionQuery():accountId(0), queryType(0), queryPos(0), queryNum(0)
	{
	}
};

struct X2ClientInstructionQueryRsp
{
	int traderId;	//交易员编号
	int	algoId;	//算法编号
	int	algoTypeId;	//算法类型编号
	char algoTypeName[ATX_LENGTH_32];	//算法类型名称
	int	orgId;	//机构编号
	int	accountId;	//账户编号
	char accountUser[ATX_LENGTH_64];	//资金账号
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxMarketType marketType;	//市场类型
	long long effectiveTime;	//开始时间
	long long expireTime;	//结束时间
	long long sysQuoteId;	//系统内母单编号
	int	transacTime;	//发生时间
	char algoParam[ATX_LENGTH_1024];	//自定义参数
	int	taskQty;	//任务数量
	int	cumQty1;	//成交数量1	
	int	cumQty2;	//成交数量2（T0卖有用）
	int	outstandingQty1;	//在途数量1
	int	outstandingQty2;	//在途数量2（T0卖有用）
	int	leavesQty1;	//剩余数量1
	int	leavesQty2;	//剩余数量2（T0卖有用）
	double	taskAmt; //任务金额
	double	cumAmt1;	//成交金额1
	double	cumAmt2;	//成交金额2（T0卖有用）
	char	note[ATX_LENGTH_256];	//备注
	int	updateTime;	//更新时间
	AtxOrderStatus	orderStatus;	//委托状态
	char	basketId[ATX_LENGTH_256];	//篮子编号
	char	clQuoteId[ATX_LENGTH_32];	//三方编号
	int 	queryPos;	//定位串
	X2ClientInstructionQueryRsp() :traderId(0), algoId(0), algoTypeId(0), orgId(0), accountId(0), effectiveTime(0),
		expireTime(0), sysQuoteId(0), transacTime(0), taskQty(0), cumQty1(0), cumQty2(0), outstandingQty1(0),
		outstandingQty2(0), leavesQty1(0), leavesQty2(0), taskAmt(0.0), cumAmt1(0.0), cumAmt2(0.0), updateTime(0),
		queryPos(0)
	{
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
		memset(algoTypeName, 0, ATX_LENGTH_32);
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(algoParam, 0, ATX_LENGTH_1024);
		memset(note, 0, ATX_LENGTH_256);
		memset(basketId, 0, ATX_LENGTH_256);
		memset(clQuoteId, 0, ATX_LENGTH_32);
	}
};

struct X2ClientQueryAtxInfo
{
	int accountId;	//账户编号
	X2ClientQueryAtxInfo():accountId(0)
	{
	}
};

struct X2ClientQueryAtxInfoRsp
{
	int traderId;	//交易员编号
	int accountId;	//账户编号
	char accountUser[ATX_LENGTH_64];	//资金账号
	int algoTypeId;	//算法类型编号
	char algoTypeName[ATX_LENGTH_32];	//算法类型名称
	int algoId;	//算法编号
	char algoName[ATX_LENGTH_64];	//算法名称
	int orgId;	//机构编号
	char algoTypeFieldsInfo[ATX_LENGTH_1024];	//算法类型字段信息
	X2ClientQueryAtxInfoRsp():traderId(0),accountId(0),algoTypeId(0),algoId(0),orgId(0)
	{
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(algoTypeName, 0, ATX_LENGTH_32);
		memset(algoName, 0, ATX_LENGTH_64);
		memset(algoTypeFieldsInfo, 0, ATX_LENGTH_1024);
	}
};

struct X2ClientInstructionCreate
{
	int algoTypeId;	//算法类型编号
	int	orgId;	//机构编号
	int	accountId;	//账户编号
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxMarketType marketType;	//市场类型
	long long effectiveTime;	//开始时间
	long long expireTime;	//结束时间
	char clQuoteId[ATX_LENGTH_32];	//三方编号
	char algoParam[ATX_LENGTH_1024];	//自定义参数
	int	algoId;	//算法编号
	char basketId[ATX_LENGTH_256];	//篮子编号
	char opstation[ATX_LENGTH_256];	//站点信息
	X2ClientInstructionCreate() :algoTypeId(0), orgId(0), accountId(0), effectiveTime(0), expireTime(0), algoId(0)
	{
		memset(symbol, 0, ATX_LENGTH_64);
		memset(clQuoteId, 0, ATX_LENGTH_32);
		memset(algoParam, 0, ATX_LENGTH_1024);
		memset(basketId, 0, ATX_LENGTH_256);
		memset(opstation, 0, ATX_LENGTH_256);
	}
};

struct X2ClientInstructionCreateRsp
{
	char clQuoteId[ATX_LENGTH_32];	//三方编号
	long long sysQuoteId;	//系统内母单编号
	char basketId[ATX_LENGTH_256];	//篮子编号
	X2ClientInstructionCreateRsp():sysQuoteId(0)
	{
		memset(clQuoteId, 0, ATX_LENGTH_32);
		memset(basketId, 0, ATX_LENGTH_256);
	}
};

struct X2ClientInstructionCancel
{
	long long sysQuoteId;	//系统内母单编号
	AtxCancelInstructionType cancelType;	//撤单类型
	X2ClientInstructionCancel():sysQuoteId(0)
	{
		cancelType = AtxCancelInstructionType::CANCEL_INSTRUCTION_TYPE_INVALID;
	}
};

struct X2ClientInstructionCancelRsp
{
	long long sysQuoteId;	//系统内母单编号
	X2ClientInstructionCancelRsp():sysQuoteId(0)
	{
	}
};

struct X2ClientInstructionReplace
{
	long long sysQuoteId;	//系统内母单编号
	long long effectiveTime;	//开始时间
	long long expireTime;	//结束时间
	char algoParam[ATX_LENGTH_1024];	//自定义参数
	X2ClientInstructionReplace():sysQuoteId(0), effectiveTime(0), expireTime(0)
	{
		memset(algoParam, 0, ATX_LENGTH_256);
	}
};

struct X2ClientInstructionReplaceRsp
{
	long long sysQuoteId;	//系统内母单编号
	long long effectiveTime;	//开始时间
	long long expireTime;	//结束时间
	char algoParam[ATX_LENGTH_1024];	//自定义参数
	X2ClientInstructionReplaceRsp() :sysQuoteId(0), effectiveTime(0), expireTime(0)
	{
		memset(algoParam, 0, ATX_LENGTH_64);
	}
};

struct X2ClientInstructionControl
{
	long long sysQuoteId;	//系统内母单编号
	int	controlType;	//0 - 暂停，1 - 恢复
	X2ClientInstructionControl():sysQuoteId(0), controlType(-1)
	{
	}
};

struct X2ClientInstructionControlRsp
{
	long long sysQuoteId;	//系统内母单编号
	int	controlType;	//0 - 暂停，1 - 恢复
	X2ClientInstructionControlRsp() :sysQuoteId(0), controlType(-1)
	{
	}
};

struct X2Topic
{
	int accountId;	//账户编号
	ReportType type;	//订阅类型
	X2Topic():accountId(0),type((ReportType)-1)
	{
	}
};

struct X2ClientInstructionReport
{
	int traderId;	//交易员编号
	int algoId;	//算法编号
	int algoTypeId;	//算法类型编号	int
	char algoTypeName[ATX_LENGTH_32];	//算法类型名称
	int orgId;	//机构编号
	int accountId;	//账户编号	int
	char accountUser[ATX_LENGTH_64];	//资金账号
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxMarketType marketType;	//市场类型
	long long effectiveTime;	//开始时间
	long long expireTime;	//结束时间	long long
	long long sysQuoteId;	//系统内母单编号
	int transacTime;	//发生时间
	char algoParam[ATX_LENGTH_1024];	//自定义参数
	int taskQty;	//任务数量	int
	int cumQty1;	//成交数量1	int
	int cumQty2;	//成交数量2（T0卖有用）	int
	int outstandingQty1;	//在途数量1	int
	int outstandingQty2;	//在途数量2（T0卖有用）	int
	int leavesQty1;	//剩余数量1	int
	int	leavesQty2;	//剩余数量2（T0卖有用）	int
	double taskAmt;	//任务金额
	double cumAmt1;	//成交金额1	double
	double cumAmt2;	//成交金额2（T0卖有用）	double
	char note[ATX_LENGTH_256];	//备注
	int updateTime;	//更新时间	int
	AtxOrderStatus orderStatus;	//委托状态
	char basketId[ATX_LENGTH_256];	//篮子编号
    char clQuoteId[ATX_LENGTH_32];    //三方编号
	X2ClientInstructionReport() :traderId(0), algoId(0), algoTypeId(0), orgId(0), accountId(0), effectiveTime(0),
		expireTime(0), sysQuoteId(0), transacTime(0), taskQty(0), cumQty1(0), cumQty2(0), outstandingQty1(0),
		outstandingQty2(0), leavesQty1(0), leavesQty2(0), taskAmt(0.0), cumAmt1(0.0), cumAmt2(0.0), updateTime(0)
	{
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
		memset(algoTypeName, 0, ATX_LENGTH_32);
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(algoParam, 0, ATX_LENGTH_1024);
		memset(note, 0, ATX_LENGTH_256);
		memset(basketId, 0, ATX_LENGTH_256);
		memset(clQuoteId, 0, ATX_LENGTH_32);
	}
};

struct X2OrderActualReport
{
	int traderId;	//交易员编号
	char accountUser[ATX_LENGTH_64];	//资金账号
	int accountId;	//账户编号
	int algoId;	//算法编号
	char brokerOrderId[ATX_LENGTH_256];	//券商委托编号
	long long sysQuoteId;	//系统内母单编号	
	long long sysOrderId;	//系统内子单编号
	AtxOrderPriceType orderType;	//订单类型
	AtxMarketType marketType;	//市场类型
	AtxPositionEffectType openClose;	//开平标志
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxSide Side;	//方向
	double orderPx;	//委托价格
	int orderQty;	//委托数量
	int transacTime;	//发生时间
	AtxOrderStatus orderStatus;	//委托状态
	int cumQty;	//成交数量
	double cumAmt;	//成交金额
	int leavesQty;	//剩余数量
	int cancelTime;	//撤单时间
	int updateTime;	//更新时间
	char note[ATX_LENGTH_256];	//备注
    char algoParam[ATX_LENGTH_1024];	//自定义参数
	X2OrderActualReport() :traderId(0),accountId(0),algoId(0),orderQty(0),transacTime(0),cumQty(0),
		cumAmt(0.0),leavesQty(0),cancelTime(0),updateTime(0)
	{
		orderType = AtxOrderPriceType::PRICE_TYPE_INVALID;
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		openClose = AtxPositionEffectType::POSITION_EFFECT_TYPE_INVALID;
		Side = AtxSide::SIDE_INVALID;
		orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(brokerOrderId, 0, ATX_LENGTH_256);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(note, 0, ATX_LENGTH_256);
        memset(algoParam, 0, ATX_LENGTH_1024);
	}
};

struct X2TradeActualReport
{
	int traderId;	//交易员编号
	char accountUser[ATX_LENGTH_64];	//资金账号
	int accountId;	//账户编号
	int algoId;	//算法编号
	char brokerOrderId[ATX_LENGTH_256];	//券商委托编号
	long long sysQuoteId;	//系统内母单编号
	long long sysOrderId;	//系统内子单编号
	AtxOrderPriceType orderType;	//订单类型
	AtxMarketType marketType;	//市场类型
	char symbol[ATX_LENGTH_64];	//证券代码
	AtxSide Side;	//方向	
	double dealPx;	//成交价格	
	double orderPx;	//委托价格	
	int orderQty;	//委托数量
	int dealQty;	//成交数量
	int dealTime;	//成交时间
	int dealType;	//成交类型
	char dealId[ATX_LENGTH_64];	//成交编号
	AtxOrderStatus orderStatus;	//委托状态
	char note[ATX_LENGTH_256];	//备注
    char algoParam[ATX_LENGTH_1024];	//自定义参数
	X2TradeActualReport():traderId(0),accountId(0),algoId(0),sysQuoteId(0),sysOrderId(0),dealPx(0.0),
		orderPx(0.0), orderQty(0), dealQty(0)
	{
		orderType = AtxOrderPriceType::PRICE_TYPE_INVALID;
		marketType = AtxMarketType::MARKET_TYPE_INVALID;
		Side = AtxSide::SIDE_INVALID;
		orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
		memset(accountUser, 0, ATX_LENGTH_64);
		memset(brokerOrderId, 0, ATX_LENGTH_256);
		memset(symbol, 0, ATX_LENGTH_64);
		memset(dealId, 0, ATX_LENGTH_64);
		memset(note, 0, ATX_LENGTH_256);
        memset(algoParam, 0, ATX_LENGTH_1024);
	}
};

struct X2SettleActualQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    AtxStrategyTypeId algoTypeId; //算法类型编号
    int algoId;	//算法编号
    char basketId[ATX_LENGTH_256];	//篮子编号
    long long sysQuoteId; //算法母单编号
    int queryPos; //定位串(默认初始为0)
    int queryNum; //单次最大数量(默认最大10000),若返回数量小于该数量说明已查询完成
    X2SettleActualQryReq() :accountId(0), beginDate(0), endDate(0), algoId(0), sysQuoteId(0), queryPos(0), queryNum(0)
    {
        algoTypeId = AtxStrategyTypeId::STRATEGYTYPE_INVALID;
        memset(basketId, 0, ATX_LENGTH_256);
    }
};

struct X2SettleActualQryRsp
{
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64]; //资金账号
    int tradeDate;	// 交易日期 20220520
    AtxSide side;	//方向
    char symbol[ATX_LENGTH_64]; //证券代码
    long long sysQuoteId; //系统内母单编号
    long long sysOrderId; //系统内子单编号
    char brokerOrderId[ATX_LENGTH_256]; //券商委托编号
    int transacTime; //发生时间
    int orderQty; //委托数量
    AtxOrderPriceType orderType; //订单类型
    double orderPx; //委托价格
    double cumAmt; //成交金额
    int cumQty; //成交数量
    int leavesQty; //剩余数量
    AtxOrderStatus orderStatus;	//委托状态
    int algoTypeId;	//算法类型编号
    int algoId;	//算法编号
    double otherFee;	//其他费用
    double fee;	//交易费用
    char basketId[ATX_LENGTH_256];	//篮子编号
    int queryPos; //定位串
    double avgPx; //成交价格
    X2SettleActualQryRsp() :accountId(0), tradeDate(0), sysQuoteId(0), sysOrderId(0), transacTime(0),
        orderQty(0),  orderPx(0.0), cumAmt(0.0), cumQty(0), leavesQty(0), algoTypeId(0), algoId(0),
        otherFee(0.0), fee(0.0), queryPos(0), avgPx(0.0)
    {
        side = AtxSide::SIDE_INVALID;
        orderType = AtxOrderPriceType::PRICE_TYPE_INVALID;
        orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(symbol, 0, ATX_LENGTH_64);
        memset(brokerOrderId, 0, ATX_LENGTH_256);
        memset(basketId, 0, ATX_LENGTH_256);
    }
};

struct X2SettleInstructionQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    AtxStrategyTypeId algoTypeId; //算法类型编号
    int algoId;	//算法编号
    char basketId[ATX_LENGTH_256]; //篮子编号
    long long sysQuoteId; //算法母单编号
    char symbol[ATX_LENGTH_64]; //证券代码;
    int queryPos; //定位串(默认初始为0)
    int queryNum; //单次最大数量(默认最大10000),若返回数量小于该数量说明已查询完成
    X2SettleInstructionQryReq():accountId(0), beginDate(0), endDate(0), algoId(0), sysQuoteId(0),
        queryPos(0), queryNum(0)
    {
        algoTypeId = AtxStrategyTypeId::STRATEGYTYPE_INVALID;
        memset(basketId, 0, ATX_LENGTH_256);
        memset(symbol, 0, ATX_LENGTH_64);
    }
};

struct X2SettleInstructionQryRsp
{
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64];	//资金账号
    int tradeDate;	// 交易日期
    AtxSide side;	//方向
    char symbol[ATX_LENGTH_64]; //证券代码;
    int taskQty;	//任务数量
    long long sysQuoteId; //系统内母单编号
    int effectiveTime; //发生时间
    int expireTime; //结束时间
    AtxOrderStatus  orderStatus; //委托状态
    int orderCount;	//子单笔数
    int cancelledCount;	//子单撤单笔数
    int cumQty1;	//成交数量
    int cumQty2;	//成交数量(T0卖有用)
    double cumAmt1;	//成交金额
    double cumAmt2;	//成交金额(T0卖有用)
    int marketStatus;	//股票状态 0-正常 -涨停 --跌停 -停牌
    double twap;	//母单twap均价
    double vwap;	//母单vwap均价
    char basketId[ATX_LENGTH_256]; //篮子编号
    double pnl;	//损益
    int outstandingQty1;	//在途数量
    int outstandingQty2;	//在途数量(T0卖有用)
    double otherFee;	//其他费用
    double fee;	//交易费用
    long long marketCumVolume;	//时段市场交易量
    double closePrice;	//收盘价
    int queryPos; //定位串
    int algoTypeId; //算法类型编号 0-非法 1-TWAP_PLUS 2-VWAP_PLUS 3-TWAP_CORE 4-VWAP_CORE 5-POV_CORE 6-PASSTHRU 7-AUCTION 8-T0
    int algoId;	//算法编号
    int cancelFlag;	// 开平标志 0-正常 5-撤单 501-盘前撤单 502-盘中撤单 503-盘后撤单 9-暂停 901-盘前暂停 902-盘中暂停 903-盘后暂停
    double avgPx;		// 成交均价
    double cumVolPercentage;	// 成交量占比
    int cumQtyBuy;		// 买成交数量
    int cumQtySell;		// 卖成交数量
    double avPxBuy;		// 买成交均价
    double avgPxSell;		// 卖成交均价
    X2SettleInstructionQryRsp() :accountId(0), tradeDate(0), taskQty(0), sysQuoteId(0), effectiveTime(0), expireTime(0), orderCount(0),
        cancelledCount(0), cumQty1(0), cumQty2(0), cumAmt1(0.0), cumAmt2(0.0), marketStatus(0), twap(0.0), vwap(0.0), pnl(0.0), outstandingQty1(0),
        outstandingQty2(0), otherFee(0.0), fee(0.0), marketCumVolume(0), closePrice(0.0), queryPos(0), algoTypeId(0), cancelFlag(0), avgPx(0.0),
        cumVolPercentage(0.0), cumQtyBuy(0), cumQtySell(0), avPxBuy(0.0), avgPxSell(0.0)
    {
        orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
        side = AtxSide::SIDE_INVALID;
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(symbol, 0, ATX_LENGTH_64);
        memset(basketId, 0, ATX_LENGTH_256);
    }
};

struct X2SettleSummaryQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    AtxStrategyTypeId algoTypeId; //算法类型编号
    int algoId;	//算法编号
    X2SettleSummaryQryReq():accountId(0), beginDate(0), endDate(0), algoId(0)
    {
        algoTypeId = AtxStrategyTypeId::STRATEGYTYPE_INVALID;
    }
};

struct X2SettleSummaryQryRsp
{
    int tradeDate;	// 交易日期
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64];	//资金账号
    AtxSide side;	//方向
    double taskAmt;	//任务金额
    double cumAmt;	//成交金额
    double pnlTwapBps;	//损益超市场的twap—bps基准
    double pnlVwapBps;	//损益超市场的vwap-bps基准
    double pnlBenchmarkBps;	//损益超市场的对标策略-bps基准
    double pnlTwapYuan;	//损益超市场的twap-元基准
    double pnlVwapYuan;	//损益超市场的vwap-元基准
    double pnlBenchmarkYuan;	//损益超市场的对标策略-元基准
    AtxStrategyTypeId algoTypeId; //算法类型编号
    int algoId;	//算法编号
    double fee;	//费用
    double otherFee;	//费用
    X2SettleSummaryQryRsp() :tradeDate(0), accountId(0), taskAmt(0.0), cumAmt(0.0), pnlTwapBps(0.0), pnlVwapBps(0.0), pnlBenchmarkBps(0.0),
        pnlTwapYuan(0), pnlVwapYuan(0.0), pnlBenchmarkYuan(0.0), algoId(0), fee(0.0), otherFee(0.0)
    {
        side = AtxSide::SIDE_INVALID;
        memset(accountUser, 0, ATX_LENGTH_64);
    }
};

struct X2SettleBasketSummaryQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    char basketId[ATX_LENGTH_256]; //篮子编号
    X2SettleBasketSummaryQryReq() :accountId(0), beginDate(0), endDate(0)
    {
        memset(basketId, 0, ATX_LENGTH_256);
    }
};

struct X2SettleBasketSummaryQryRsp
{
    int tradeDate;	// 交易日期
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64]; //资金账号
    char basketId[ATX_LENGTH_256];	//篮子编号
    AtxSide side;	//方向
    double taskAmt;	//任务金额
    double cumAmt;	//成交金额
    double pnlTwapBps;	//损益超市场的twap—bps基准
    double pnlVwapBps;	//损益超市场的vwap-bps基准
    double pnlBenchmarkBps;	//损益超市场的对标策略-bps基准
    double pnlTwapYuan;	//损益超市场的twap-元基准
    double pnlVwapYuan;	//损益超市场的vwap-元基准
    double pnlBenchmarkYuan;	//损益超市场的对标策略-元基准
    double fee;	//费用
    double otherFee;	//费用
    X2SettleBasketSummaryQryRsp() :tradeDate(0), accountId(0), taskAmt(0.0), cumAmt(0.0), pnlTwapBps(0.0), pnlVwapBps(0.0), pnlBenchmarkBps(0.0),
        pnlTwapYuan(0.0), pnlVwapYuan(0.0), pnlBenchmarkYuan(0.0), fee(0.0), otherFee(0.0)
    {
        side = AtxSide::SIDE_INVALID;
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(basketId, 0, ATX_LENGTH_256);
    }
};

struct X2SettleT0SummaryQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    X2SettleT0SummaryQryReq() :accountId(0), beginDate(0), endDate(0)
    {
    }
};

struct X2SettleT0SummaryQryRsp
{
    int tradeDate;	// 交易日期
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64]; //资金账号
    double taskAmt;	//任务金额
    double cumAmt;	//成交金额
    double cancelRate;	//撤单比例
    double usedRate;	//利用率
    double pnlToday;	//当日收益
    double pnlClearYesterday;	//隔夜仓收益
    double pnlAggregate;	//当日最终收益
    double totalFilledAmt;		// 双边成交金额
    X2SettleT0SummaryQryRsp():tradeDate(0), accountId(0), taskAmt(0.0), cumAmt(0.0), cancelRate(0.0), usedRate(0.0), pnlToday(0.0),
        pnlClearYesterday(0.0), pnlAggregate(0.0), totalFilledAmt(0.0)
    {
        memset(accountUser, 0, ATX_LENGTH_64);
    }
};

struct X2SettleT0OverNightQryReq
{
    int accountId;	//账户编号
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    X2SettleT0OverNightQryReq() :accountId(0), beginDate(0), endDate(0)
    {
    }
};

struct X2SettleT0OverNightQryRsp
{
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64]; //资金账号
    int tradeDate;	// 交易日期
    char symbol[ATX_LENGTH_64]; //证券代码;
    int untradeQty;	//未交易股数
    double closePrice;	//当天收盘价
    double untradeMarketValue;	//未成交收盘市值
    X2SettleT0OverNightQryRsp():accountId(0), tradeDate(0), untradeQty(0), closePrice(0.0), untradeMarketValue(0.0)
    {
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(symbol, 0, ATX_LENGTH_64);
    }
};

struct X2TradeActualQuery
{
    int accountId;	//账户编号
    char symbol[ATX_LENGTH_64];	//证券代码
    AtxMarketType marketType;	//市场
    long long sysOrderId;	//系统内委托编号
    char queryPos[ATX_LENGTH_256];	//定位串（默认初始为空）
    int queryNum;	//单次最大数量（默认最大2000）
    X2TradeActualQuery() :accountId(0), sysOrderId(0), queryNum(0)
    {
        marketType = AtxMarketType::MARKET_TYPE_INVALID;
        memset(symbol, 0, ATX_LENGTH_64);
        memset(queryPos, 0, ATX_LENGTH_256);
    }
};

struct X2DealActualQueryRsp
{
    int traderId;	//交易员编号
    char accountUser[ATX_LENGTH_64];	//资金账号
    int accountId;	//账户编号
    int algoId; //算法应用编号
    char brokerOrderId[ATX_LENGTH_256];	//券商委托编号
    long long sysQuoteId;	//系统内母单编号
    long long sysDealId; //系统内成交编号
    long long sysOrderId;	//系统内委托编号
    AtxOrderPriceType orderType;	//订单类型
    AtxMarketType marketType;	//市场类型
    char symbol[ATX_LENGTH_64];	//证券代码
    AtxSide side;	//方向	
    double dealPx;	//成交价格	
    int dealQty;	//成交数量
    int dealTime;	//成交时间
    int dealType;	//成交类型
    char dealId[ATX_LENGTH_64];	//成交编号
    int queryPos; //定位串
    X2DealActualQueryRsp() :traderId(0), accountId(0), algoId(0), sysQuoteId(0), sysDealId(0), sysOrderId(0), dealPx(0.0), dealQty(0), dealTime(0), dealType(0), queryPos(0)
    {
        orderType = AtxOrderPriceType::PRICE_TYPE_INVALID;
        marketType = AtxMarketType::MARKET_TYPE_INVALID;
        side = AtxSide::SIDE_INVALID;
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(brokerOrderId, 0, ATX_LENGTH_256);
        memset(symbol, 0, ATX_LENGTH_64);
        memset(dealId, 0, ATX_LENGTH_64);
    }
};

struct X2ClientHistoryInstructionQuery
{
    int beginDate; //开始日期,20220520
    int endDate;	//结束日期,20220520
    int	queryPos;	//定位串（默认初始为0）
    int	queryNum;	//单次最大数量（默认最大2000）
    X2ClientHistoryInstructionQuery() :beginDate(0), endDate(0), queryPos(0), queryNum(0)
    {
    }
};

struct X2ClientHistoryInstructionQueryRsp
{
    int tradeDate;	//交易日
    int traderId;	//交易员编号
    int algoId;	//算法编号
    int algoTypeId;	//算法类型编号
    char algoTypeName[ATX_LENGTH_32];	//算法类型名称
    int orgId;	//机构编号
    int accountId;	//账户编号
    char accountUser[ATX_LENGTH_64];	//资金账号
    char symbol[ATX_LENGTH_64];	//证券代码
    AtxMarketType marketType;	//市场类型
    long long effectiveTime;	//开始时间
    long long expireTime;	//结束时间
    long long sysQuoteId;	//系统内母单编号
    int	transacTime;	//发生时间
    char algoParam[ATX_LENGTH_1024];	//自定义参数
    int taskQty;	//任务数量
    int cumQty1;	//成交数量1	
    int cumQty2;	//成交数量2（T0卖有用）
    int outstandingQty1;	//在途数量1
    int outstandingQty2;	//在途数量2（T0卖有用）
    int leavesQty1;	//剩余数量1
    int leavesQty2;	//剩余数量2（T0卖有用）
    double taskAmt; //任务金额
    double cumAmt1;	//成交金额1
    double cumAmt2;	//成交金额2（T0卖有用）
    char note[ATX_LENGTH_256];	//备注
    int updateTime;	//更新时间
    AtxOrderStatus orderStatus;	//委托状态
    char basketId[ATX_LENGTH_256];	//篮子编号
    char clQuoteId[ATX_LENGTH_32];	//三方编号
    int  queryPos;	//定位串
    X2ClientHistoryInstructionQueryRsp() :tradeDate(0), traderId(0), algoId(0), algoTypeId(0), orgId(0), accountId(0), effectiveTime(0),
        expireTime(0), sysQuoteId(0), transacTime(0), taskQty(0), cumQty1(0), cumQty2(0), outstandingQty1(0),
        outstandingQty2(0), leavesQty1(0), leavesQty2(0), taskAmt(0.0), cumAmt1(0.0), cumAmt2(0.0), updateTime(0),
        queryPos(0)
    {
        marketType = AtxMarketType::MARKET_TYPE_INVALID;
        orderStatus = AtxOrderStatus::ORDER_STATUS_INVALID;
        memset(algoTypeName, 0, ATX_LENGTH_32);
        memset(accountUser, 0, ATX_LENGTH_64);
        memset(symbol, 0, ATX_LENGTH_64);
        memset(algoParam, 0, ATX_LENGTH_1024);
        memset(note, 0, ATX_LENGTH_256);
        memset(basketId, 0, ATX_LENGTH_256);
        memset(clQuoteId, 0, ATX_LENGTH_32);
    }
};

struct X2AcctIdsByAcctUserQuery
{
    char accountUser[ATX_LENGTH_64];	//资金账号
    X2AcctIdsByAcctUserQuery()
    {
        memset(accountUser, 0, ATX_LENGTH_64);
    }
};

struct X2AcctIdsByAcctUserQueryRsp
{
    int accountId; //账户编号
    char accountUser[ATX_LENGTH_64]; //账户编号
    int brokerId; //通道编号
    X2AcctIdsByAcctUserQueryRsp() :accountId(0), brokerId(0)
    {
        memset(accountUser, 0, ATX_LENGTH_64);
    }
};

struct X2RspInfoField
{
	int errCode;	//错误码（0成功，非0失败）
	char errMsg[ATX_LENGTH_256];	//错误信息
	X2RspInfoField():errCode(-1)
	{
		memset(errMsg, 0, ATX_LENGTH_256);
	}
};