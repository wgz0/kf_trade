﻿#pragma once

#include "X2AtxStruct.h"

// 响应类Spi接口
class X2ClientTradeSpi
{
public:
    //建立连接成功后回调
    virtual void OnConnect() = 0;
    //连接断开时回调
    virtual void OnDisconect() = 0;
	//登录应答
	virtual void OnRspLogin(X2LoginRsp* pRspLogin, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//资金账号查询应答
	virtual void OnRspQueryAccountInfo(X2FundAccountInfoRsp* pRspAccount, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
    //通过资金资产账户查询资金账号应答
    virtual void OnRspQueryAccountId(X2QueryAccountIdRsp* pRspQueryAccountId, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//账户资金查询应答
	virtual void OnRspBalance(X2BalanceRsp* pBalance, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//资金账号密码修改应答
	virtual void OnRspAccountChangePwd(X2AccountChangePwdRsp* pAccountChangePwd, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//股票持仓查询应答
	virtual void OnRspStockPosition(X2StockPositionRsp* pPosition, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//子单委托回报主推
	virtual void OnRtnOrderReport(X2OrderActualReport* orderReport) = 0;
	//订阅主题响应
	virtual void OnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID) = 0;
    //取消订阅主题响应
    virtual void OnUnSubTopic(X2Topic* topic, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//子单成交回报主推
	virtual void OnRtnTradeReport(X2TradeActualReport* tradeReport) = 0;
	//母单委托回报主推
	virtual void OnRtnInstructionReport(X2ClientInstructionReport* pReport) = 0;
	//子单订单查询应答
	virtual void OnRspQueryOrderActual(X2OrderActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单订单查询应答
	virtual void OnRspQueryOrderInstruction(X2ClientInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//算法类型查询应答
	virtual void OnRspQueryAlgoType(X2ClientQueryAtxInfoRsp* pRspAtx, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单下单应答
	virtual void OnRspInstructionCreate(X2ClientInstructionCreateRsp* pRspCreate, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单撤单应答
	virtual void OnRspInstructionCancel(X2ClientInstructionCancelRsp* pRspCancel, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单改单应答
	virtual void OnRspInstructionReplace(X2ClientInstructionReplaceRsp* pRspReplace, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单暂停/恢复应答
	virtual void OnRspInstructionControl(X2ClientInstructionControlRsp* pRspControl, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算子单查询
	virtual void OnRspSettleActualQry(X2SettleActualQryRsp* pSettleActualQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算母单查询
	virtual void OnRspSettleInstructionQry(X2SettleInstructionQryRsp* pSettleSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算汇总查询
	virtual void OnRspSettleSummaryQry(X2SettleSummaryQryRsp* pSettleInstructionQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算篮子汇总查询
	virtual void OnRspSettleBasketSummaryQry(X2SettleBasketSummaryQryRsp* pSettleBasketSummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算T0汇总查询
	virtual void OnRspSettleT0SummaryQry(X2SettleT0SummaryQryRsp* pSettleT0SummaryQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//结算隔夜仓查询
	virtual void OnRspSettleT0OverNightQry(X2SettleT0OverNightQryRsp* pSettleT0OverNightQry, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//子单成交查询应答
	virtual void OnRspQueryDealActual(X2DealActualQueryRsp* pRspActual, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//母单历史订单查询应答
	virtual void OnRspQueryHistoryOrderInstruction(X2ClientHistoryInstructionQueryRsp* pRspInstruction, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;
	//一地多户查询应答
	virtual void OnRsQueryAcctIdsByAcctUser(X2AcctIdsByAcctUserQueryRsp* pAcctIdsByAcctUser, int nCount, X2RspInfoField* pRspInfo, int nRequestID) = 0;

	virtual ~X2ClientTradeSpi() {};
};