#ifndef ATX_ENUM_H
#define ATX_ENUM_H
#pragma once

#ifndef ATX_LENGTH_32
#define ATX_LENGTH_32 32
#endif

#ifndef ATX_LENGTH_64
#define ATX_LENGTH_64 64
#endif

#ifndef ATX_LENGTH_256
#define ATX_LENGTH_256 256
#endif

#ifndef ATX_LENGTH_512
#define ATX_LENGTH_512 512
#endif

#ifndef ATX_LENGTH_1024
#define ATX_LENGTH_1024 1024
#endif

#ifndef ATX_ALGO_T0
#define ATX_ALGO_T0 "T0"
#endif

// 撤单类型
enum class AtxCancelInstructionType
{
	CANCEL_INSTRUCTION_TYPE_INVALID = 0,//无效值
	CANCEL_INSTRUCTION_STOPTRADE = 1,   //停止交易
	CANCEL_INSTRUCTION_STOPINIT = 2,    //禁开
};

// 交易所类型
enum class AtxMarketType
{
	MARKET_TYPE_INVALID = 0,  //无效值
	MARKET_TYPE_SZ = 1,       //深证
	MARKET_TYPE_SH = 2,       //上海
	MARKET_TYPE_CFE = 3,      //中金所
	MARKET_TYPE_SHF = 4,      //上期所
	MARKET_TYPE_DCE = 5,      //大连商品
	MARKET_TYPE_ZCE = 6,      //郑州商品
	MARKET_TYPE_INE = 7,      //能源
};

// 委托状态
enum class AtxOrderStatus
{
    ORDER_STATUS_INVALID = 0,               //无效值
    ORDER_STATUS_PENDINGNEW = 1,            //未报
    ORDER_STATUS_NEW = 2,                   //已报
    ORDER_STATUS_PARTFILLED = 3,            //部成
    ORDER_STATUS_FILLED = 4,                //已成
    ORDER_STATUS_CANCELED = 5,              //已撤
    ORDER_STATUS_PENDINGCANCEL = 6,         //待撤
    ORDER_STATUS_STOPPED = 7,               //内部废单
    ORDER_STATUS_REJECTED = 8,              //拒绝
    ORDER_STATUS_SUSPEND = 9,               //暂停
    ORDER_STATUS_EXPIRED = 10,              //过期
    ORDER_STATUS_PARTCANCELED = 11,         //部分撤销
    ORDER_STATUS_FORCECANCELED = 12,        //全部销毁
    ORDER_STATUS_FORCEPARTCANCELED = 13,    //部分销毁
    ORDER_STATUS_PENDINGSUSPEND = 14,       //待暂停
    ORDER_STATUS_PENDINGSTART = 15,         //待启动
    ORDER_STATUS_PENDINGNOOPEN = 16,        //待禁开
    ORDER_STATUS_NOOPENED = 17,             //禁开(有敞口)
    ORDER_STATUS_PENDINGMODIFY = 18,        //待修改
    ORDER_STATUS_PENDINGNEWSUB = 19,        //待执行
    ORDER_STATUS_FORCECANCELED1 = 20,       //已销毁
    ORDER_STATUS_FORCECANCELEDFAILED = 21,  //销毁失败
    ORDER_STATUS_FORCEPENDINGCANCEL = 22,   //待销毁
};

// 委托价格类型
enum class AtxOrderPriceType
{
	PRICE_TYPE_INVALID = 0,            //未知或者无效价格类型
	PRICE_TYPE_LIMIT = 1,              //限价单-沪/深/沪期权（除普通股票业务外，其余业务均使用此种类型）
	PRICE_TYPE_BEST_OR_CANCEL = 2,     //即时成交剩余转撤销，市价单-深/沪期权
	PRICE_TYPE_BEST5_OR_LIMIT = 3,     //最优五档即时成交剩余转限价，市价单-沪
	PRICE_TYPE_BEST5_OR_CANCEL = 4,    //最优5档即时成交剩余转撤销，市价单-沪深
	PRICE_TYPE_ALL_OR_CANCEL = 5,      //全部成交或撤销;市价单-深/沪期权
	PRICE_TYPE_FORWARD_BEST = 6,       //本方最优，市价单-深
	PRICE_TYPE_REVERSE_BEST_LIMIT = 7, //对方最优剩余转限价，市价单-深/沪期权
	PRICE_TYPE_LIMIT_OR_CANCEL = 8,    //期权限价申报FOK
};

// 委托方向
enum class AtxSide
{
	SIDE_INVALID = 0,   //非法方向
	SIDE_BUY = 1,       //买（新股申购，ETF买，配股，信用交易中担保品买）
	SIDE_SELL = 2,      //卖（逆回购，ETF卖，信用交易中担保品卖）
	SIDE_CLOSE_BUY = 3, //买券还券
	SIDE_SHORT_SELL = 4,//融券卖出
	SIDE_SHORT_BUY = 5, //融资买入
	SIDE_CLOSE_SELL = 6,//卖券还款
};

// 开平标志
enum class AtxPositionEffectType
{
	POSITION_EFFECT_TYPE_INVALID = 0,   //无效值
	POSITION_EFFECT_OPEN = 1,           //开仓
	POSITION_EFFECT_CLOSE = 2,          //平仓
	POSITION_EFFECT_FORCECLOSE = 3,     //强平
	POSTION_EFFECT_CLOSETODAY = 4,      //平今
	POSITION_EFFECT_CLOSEYESTERDAY = 5, //平昨
};

// 证券类别
enum class AtxSecurityType
{
	SECURITY_TYPE_INVALID = 0,  //无效值
	SECURITY_TYPE_CS = 1,       //普通股票
	SECURITY_TYPE_ETF = 2,      //ETF
	SECURITY_TYPE_CB = 3,       //可转债
	SECURITY_TYPE_RB = 4,       //债券回购
	SECURITY_TYPE_IDX = 5,      //指数
	SECURITY_TYPE_IF = 6,       //股指期货
	SECURITY_TYPE_TF = 7,       //国债期货
	SECURITY_TYPE_CF = 8,       //商品期货
	SECURITY_TYPE_NS = 9,       //新股申购
	SECURITY_TYPE_NB = 10,      //可转债申购
};

// 系统时间类型
enum class AtxEventType
{
	EVENT_TYPE_INVALID = 0,     //无效值
	EVENT_TYPE_DBUPDATE = 1,    //数据库变动
	EVENT_TYPE_USERINFOCHILD = 2,
	EVENT_TYPE_USERINFOPARENT = 3,
	EVENT_TYPE_NEWCLIENT = 4,
};

// 账户状态
enum class AtxAccountStatus
{
	ACCOUNT_STATUS_INVALID = 0, //无效值
	ACCOUNT_ONLINE = 1,         //在线
	ACCOUNT_OFFLINE = 2,        //离线
};

// 模块类型
enum class AtxModuleType
{
	MODULE_TYPE_INVALID = 0,    //无效值
	MODULE_TYPE_ALGO = 1,       //算法提供商
	MODULE_TYPE_ADAPTOR = 2,    //adaptor（账号变动）
    MODULE_TYPE_RISK = 3,       //风控
};

enum class AtxSysStatus
{
	GT_INVALID = 0,    // 无效值
    GT_INACTIVE = 1,   // 未激活
    GT_CHECKIN = 2,    // 校验中
    GT_NORMAL = 3,     // 正常
    GT_OFFLINE = 4,    // 离线
    GT_ONFROZEN = 5,   // 待冻结
    GT_FROZEN = 6,     // 冻结
    GT_ONCANCELLATION = 7, // 待注销
    GT_CANCELLATION = 8,// 注销
};

//预埋单状态
enum class ParkedOrderStatus
{
    PARKED_ORDER_STATUS_INVALID = 0,    //无效值
    PARKED_ORDER_STATUS_START = 1,      //启动
    PARKED_ORDER_STATUS_STOP = 2,       //停止
    PARKED_ORDER_STATUS_CANCEL = 3,     //撤销
    PARKED_ORDER_STATUS_DONE = 4,       //完成
    PARKED_ORDER_STATUS_FAILED = 5,     //失败
};

//价格类型
enum class PriceType
{
    PRICE_TYPE_INVALID = 0,     //无效值
    PRICE_TYPE_FIXED_PRICE = 1, //固定价
    PRICE_TYPE_LAST_PRICE = 2,  //最新价
    PRICE_TYPE_BID_PRICE1 = 3,  //买一价
    PRICE_TYPE_BID_PRICE2 = 4,  //买二价
    PRICE_TYPE_BID_PRICE3 = 5,  //买三价
    PRICE_TYPE_BID_PRICE4 = 6,  //买四价
    PRICE_TYPE_BID_PRICE5 = 7,  //买五价
    PRICE_TYPE_ASK_PRICE1 = 8,  //卖一价
    PRICE_TYPE_ASK_PRICE2 = 9,  //卖二价
    PRICE_TYPE_ASK_PRICE3 = 10, //卖三价
    PRICE_TYPE_ASK_PRICE4 = 11, //卖四价
    PRICE_TYPE_ASK_PRICE5 = 12, //卖五价
    PRICE_TYPE_LIMIT_HIGH_PRICE = 13,   //涨停价
    PRICE_TYPE_LIMIT_LOW_PRICE = 14,    //跌停价
};

//触发价格条件
enum class TriggerPriceCondition
{
    TPC_INVALID = 0, //无效值
    TPC_GREATER = 1, //大于
    TPC_GREATER_EQUEL = 2, //大于等于
    TPC_LESS = 3, //小于
    TPC_LESS_EQUEL = 4, //小于等于
    TPC_EQUEL = 5, //等于
};

// 订阅类型
enum class ReportType
{
    REPORT_TYPE_DEAL = 1,          //成交主推
    REPORT_TYPE_ORDER,             //委托主推
    REPORT_TYPE_ALGOORDER,         //母单主推
    REPORT_TYPE_FUNDR,             //资金主推
    REPORT_TYPE_HOLD,              //持仓主推
    REPORT_TYPE_ACCOUNT            //资金账号状态主推
};

// 交易员状态
enum class AtxTraderStatus
{
	TRADER_STATUS_INVALID = 0,  // 无效值
    TRADER_STATUS_NORMAL = 1,   // 正常
    TRADER_STATUS_FREEZE = 2,   // 冻结
    TRADER_STATUS_LOCK = 3,     // 锁定
    TRADER_STATUS_LOGOUT = 4,   // 注销
};

// 交易员类型
enum class AtxTraderType
{
	TRADER_TYPE_INVALID = 0,    // 无效值
    TRADER_TYPE_USER = 1,       // 交易员
};

// 账户类型
enum class AtxAccountType
{
	ACCOUNT_TYPE_INVALID = 0,   // 无效值
    ACCOUNT_TYPE_NORMAL = 1,    // 普通账户
	ACCOUNT_TYPE_FUTURE = 2,    // 期货账户
};

// 资产属性
enum class AtxAssetProp
{
	ASSET_PROP_INVALID = 0,     // 无效值
    ASSET_PROP_STOCK = 1,       // 证券交易
    ASSET_PROP_MARGIN = 2,      // 融资融券
    ASSET_PROP_OPTION = 3,      // 股票期权
};

// 证件类型
enum class AtxIdKind
{
	ID_KIND_INVALID = 0,        // 无效值
    ID_KIND_CARD = 1,           // 身份证
    ID_KIND_OTHER = 2,          // 其它证件 
};

enum class AtxStrategyTypeId {
    STRATEGYTYPE_INVALID = 0,         // 无效值
    STRATEGYTYPE_TWAP_PLUS = 101,     // TWAP_PLUS
    STRATEGYTYPE_VWAP_PLUS = 102,     // TWAP_CORE
    STRATEGYTYPE_TWAP_CORE = 103,     // VWAP_PLUS
    STRATEGYTYPE_VWAP_CORE = 104,     // VWAP_CORE
    STRATEGYTYPE_POV_CORE = 105,      // POV_CORE
    STRATEGYTYPE_T0 = 106,            // T0
    STRATEGYTYPE_PASSTHRU = 201       // PASSTHRU
};

#endif
